﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Amphi.Models;
using Amphi.Views;

namespace Amphi.Controllers
{
    public class HtmlWidgetViewController
    {
        private List<HtmlWidgetView> forms = new List<HtmlWidgetView>();

        #region Index
        public List<HtmlWidgetView> Index
        {
            get
            {
                return this.forms;
            }
        }
        #endregion

        #region Create
        public void Create(HtmlWidget newWidget)
        {
            if (this.Find(newWidget).Count == 0)
            {
                HtmlWidgetView newWidgetView = new HtmlWidgetView(newWidget);
                this.forms.Add(newWidgetView);
            }
        }
        #endregion

        #region Find
        public List<HtmlWidgetView> Find(HtmlWidget widgetOfForm)
        {
            List<HtmlWidgetView> found = new List<HtmlWidgetView>();

            foreach (HtmlWidgetView f in this.forms)
            {
                if (f.ActiveWidget.Equals(widgetOfForm))
                {
                    found.Add(f);
                }
            }

            return found;
        }

        public List<HtmlWidgetView> Find(Mode mode)
        {
            List<HtmlWidgetView> found = new List<HtmlWidgetView>();
            foreach (HtmlWidget w in mode.Widgets.Index)
            {
                found.Add(this.Find(w)[0]);
            }

            return found;
        } 
        #endregion

        #region Update
        public void Update(HtmlWidgetView oldWidget, HtmlWidgetView newWidget)
        {
            try
            {
                this.forms.Add(newWidget);
                this.forms.Remove(oldWidget);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Destroy
        public void Destroy(HtmlWidgetView targetWidget)
        {
            targetWidget.Close();
            this.forms.Remove(targetWidget);
        }

        public void DestroyAll()
        {
            this.forms.Clear();
        }
        #endregion
    }
}
