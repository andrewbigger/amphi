﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amphi.Helpers;
using Amphi.Models;
using Amphi.Views;

namespace Amphi.Controllers
{
   public static class ApplicationController
    {
       private static ModeController modes = new ModeController();
       private static HtmlWidgetViewController widgetViews = new HtmlWidgetViewController();
       private static Mode activeMode;

       #region Public Properties
       public static ModeController Modes
       {
           get
           {
               return modes;
           }
       }

       public static HtmlWidgetViewController WidgetViews
       {
           get
           {
               return widgetViews;
           }
       }

       public static Mode ActiveMode
       {
           get
           {
               activeMode = modes.Find(Properties.Settings.Default.ActiveMode)[0];
               return activeMode;
           }
       }

       public static string ModeDataPath
       {
           get
           {
               return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Amphi Dashboard\\modes.bin";
           }
       }
       #endregion

       #region Public Methods
       public static void Load()
       {
           try
           {
               ModeController loadedModes = (ModeController)IOHelper.Load(ModeDataPath);
               modes = loadedModes;
           }
           catch (Exception e)
           {
               throw e;
           }
       }

       public static void Save()
       {
            IOHelper.Save(ModeDataPath, modes);
       }
       #endregion

       #region Create

       public static void Create(string mode, string name, string url)
       {
           Mode targetMode = Find(mode);
           HtmlWidget newWidget;

           try
           {
               Point defaultStartLocation = new Point(0, 0);
               targetMode.Widgets.Create(name, url, defaultStartLocation);
               newWidget = targetMode.Widgets.Find(name)[0];
               widgetViews.Create(newWidget);
               Save();
           }
           catch (Exception e)
           {
               throw e;
           }
       }

       public static HtmlWidget CreateStubWidget(string mode)
       {
          Create(mode, "stub", "http://stub.com");
          return Find(mode, "stub");
       }

       public static Mode CreateStubMode()
       {
           Modes.Create("stub");
           return Find("stub");
       }
        #endregion

       #region Destroy
        public static void Destroy(string mode)
        {
            try
            {
                Mode targetMode = Find(mode);
                DestroyWidgets(targetMode);
                modes.Destroy(targetMode);
                Save();
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new MissingItemException("Cannot find Mode", e.InnerException);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void DestroyStubWidget(string mode)
        {
            Destroy(mode, "stub");
        }

        public static void DestroyStubMode()
        {
            Destroy("stub");
        }
        
        public static void Destroy(string mode, string widget)
        {
            Mode targetMode = Find(mode);

            try
            {
                HtmlWidget targetWidget = targetMode.Widgets.Find(widget)[0];
                CloseAndDestroyHtmlViews(targetWidget);
                targetMode.Widgets.Destroy(targetWidget);
                Save();
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new MissingItemException("Cannot find Widget", e.InnerException);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

       #region Find
        public static Mode Find(string mode)
        {
            try
            {
                return modes.Find(mode)[0];
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new MissingItemException("Cannot find Mode", e.InnerException);
            }
        }

        public static HtmlWidget Find(string mode, string widget)
        {
            Mode targetMode = Find(mode);
            
            try
            {
                return targetMode.Widgets.Find(widget)[0];
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new MissingItemException("Cannot find Widget", e.InnerException);
            }
        }

        public static HtmlWidgetView FindWidgetView(string mode, string widget)
        {
            Mode targetMode = Find(mode);
            
            try
            {
                return widgetViews.Find(targetMode.Widgets.Find(widget)[0])[0];
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new MissingItemException("Cannot find Widget", e.InnerException);
            }
        }
        #endregion

       #region Private Helpers
        private static void DestroyWidgets(Mode targetMode)
        {
            foreach (HtmlWidget w in targetMode.Widgets.Index)
            {
                CloseAndDestroyHtmlViews(w);
            }

            targetMode.Widgets.DestroyAll();
        }

        private static void CloseAndDestroyHtmlViews(HtmlWidget targetWidget)
        {
            List<HtmlWidgetView> views = ApplicationController.WidgetViews.Find(targetWidget);
            foreach (HtmlWidgetView v in views)
            {
                ApplicationController.WidgetViews.Destroy(v);
            }
        } 
        #endregion
    }
}
