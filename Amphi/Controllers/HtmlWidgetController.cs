﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amphi.Models;

namespace Amphi.Controllers
{
    [Serializable]
    public class HtmlWidgetController
    {
        private List<HtmlWidget> widgets = new List<HtmlWidget>();

        #region Index
        public List<HtmlWidget> Index
        {
            get
            {
                return this.widgets;
            }
        }
        #endregion

        #region Create
        public void Create(string name, string url, Point location)
        {
            // TODO: Throw error if it already exists
            try
            {
                HtmlWidget newWidget = new HtmlWidget(name, url, location);
                this.widgets.Add(newWidget);
            }
            catch (Exception e) 
            { 
                throw e; 
            }
        }
        #endregion

        #region Find
        public List<HtmlWidget> Find(string nameQuery)
        {
            List<HtmlWidget> found = new List<HtmlWidget>();

            foreach (HtmlWidget w in this.widgets)
            {
                if (w.Name.Equals(nameQuery))
                {
                    found.Add(w);
                }
            }

            return found;
        }
        #endregion

        #region Update
        public void Update(HtmlWidget oldWidget, HtmlWidget newWidget)
        {
            try
            {
                this.widgets.Add(newWidget);
                this.Destroy(oldWidget);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Destroy
        public void Destroy(HtmlWidget targetWidget)
        {
            this.widgets.Remove(targetWidget);
        }

        public void DestroyAll()
        {
            this.widgets.Clear();
        }
        #endregion
    }
}
