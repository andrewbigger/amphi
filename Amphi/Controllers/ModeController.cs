﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amphi.Models;

namespace Amphi.Controllers
{
    [Serializable]
    public class ModeController
    {
        private List<Mode> modes = new List<Mode>();

        #region Index
        public List<Mode> Index
        {
            get
            {
                return this.modes;
            }
        }
        #endregion

        #region Create
        public void Create(string name)
        {
            // do not create if dupe
            try
            {
                Mode newMode = new Mode(name);
                this.modes.Add(newMode);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Find
        public List<Mode> Find(string nameQuery)
        {
            List<Mode> found = new List<Mode>();

            foreach (Mode m in this.modes)
            {
                if (m.Name.Equals(nameQuery))
                {
                    found.Add(m);
                }
            }

            return found;
        }
        #endregion

        #region Update
        public void Update(Mode oldMode, Mode newMode)
        {
            try
            {
                this.modes.Add(newMode);
                this.Destroy(oldMode);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Destroy
        public void Destroy(Mode targetMode)
        {
            targetMode.Widgets.DestroyAll();
            this.modes.Remove(targetMode);
        }

        public void Destroy(string modeQuery)
        {
            try
            {
                Mode targetMode = this.Find(modeQuery)[0];
                targetMode.Widgets.DestroyAll();
                this.modes.Remove(targetMode);
            }
            catch (IndexOutOfRangeException ex)
            {
                throw ex;
            }
        }

        public void DestroyAll()
        {
            this.modes.Clear();
        }
        #endregion
    }
}
