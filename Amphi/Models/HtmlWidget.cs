﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amphi.Helpers;

namespace Amphi.Models
{
    [Serializable]
    public class HtmlWidget
    {
        private string name;
        private Uri url;
        private Point location;
        private Size size = new Size(500, 500);
        private bool locked;

        #region Constructor
        public HtmlWidget(string widgetName, string widgetUrl, Point widgetLocation, bool locked = false)
        {
            this.Name = widgetName;
            this.SetUrl = widgetUrl;
            this.Location = widgetLocation;
        }
        #endregion

        #region Public Properties
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (ValidName(value) == true)
                {
                    this.name = value;
                }
                else 
                { 
                    throw new InvalidWidgetNameException(value); 
                }
            }
        }

        public Uri GetUrl
        {
            get
            {
                return this.url;
            }
        }

        public string SetUrl
        {
            set
            {
                if (ValidUrl(value))
                {
                    this.url = new Uri(value);
                }
                else 
                { 
                    throw new InvalidWidgetUrlException(value); 
                }
            }
        }

        public Point Location
        {
            get
            {
                return this.location;
            }

            set
            {
                this.location = value;
            }
        }

        public Size Size
        {
            get
            {
                return this.size;
            }

            set
            {
                this.size = value;
            }
        }

        public bool Locked
        {
            get
            {
                return locked;
            }

            set
            {
                this.locked = value;
            }
        }
        #endregion

        #region Private Methods
        private static bool ValidName(string newName)
        {
            if (newName.Trim() == string.Empty) 
            { 
                return false; 
            }

            return true;
        }

        private static bool ValidUrl(string newUrl)
        {
            try
            {
                Uri urlResult;
                bool result = Uri.TryCreate(newUrl, UriKind.Absolute, out urlResult);
                if (result && urlResult.Scheme == Uri.UriSchemeHttp) 
                { 
                    return true; 
                }
                else 
                { 
                    return false; 
                }
            }
            catch (Exception) 
            {
                return false;
            }
        }
        #endregion
    }
}
