﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amphi.Helpers;
using Amphi.Models;

namespace Amphi.Models
{
    class HtmlWidget : Widget
    {
        private Uri url;

        #region Constructor
        public HtmlWidget(string widgetName, string widgetUrl, Point widgetLocation)
        {
            this.Name = widgetName;
            this.SetUrl = widgetUrl;
            this.Location = widgetLocation;
        }
        #endregion

        #region Public Properties
        public Uri GetUrl
        {
            get
            {
                return this.url;
            }
        }

        public string SetUrl
        {
            set
            {
                if (ValidUrl(value))
                {
                    this.url = new Uri(value);
                }
                else 
                { 
                    throw new InvalidWidgetUrlException(value); 
                }
            }
        }
        #endregion

        #region Private Methods
        private static bool ValidUrl(string newUrl)
        {
            try
            {
                Uri urlResult;
                bool result = Uri.TryCreate(newUrl, UriKind.Absolute, out urlResult);
                if (result && urlResult.Scheme == Uri.UriSchemeHttp) 
                { 
                    return true; 
                }
                else 
                { 
                    return false; 
                }
            }
            catch (Exception) 
            {
                return false;
            }
        }
        #endregion
    }
}
