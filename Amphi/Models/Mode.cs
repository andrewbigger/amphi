﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amphi.Controllers;
using Amphi.Helpers;

namespace Amphi.Models
{
    [Serializable]
    public class Mode
    {
        private string name;
        private HtmlWidgetController widgets;

        #region Constructor
        public Mode(string modeName)
        {
            this.Name = modeName;
            this.Widgets = new HtmlWidgetController();
        }
        #endregion

        #region Public Properties
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (ValidName(value) == true)
                {
                    this.name = value;
                }
                else
                {
                    throw new InvalidModeNameException(value);
                }
            }
        }

        public HtmlWidgetController Widgets
        {
            get
            {
                return this.widgets;
            }

            set
            {
                this.widgets = value;
            }
        }
        #endregion

        #region Private Methods
        private static bool ValidName(string newName)
        {
            if (newName.Trim() == string.Empty)
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}
