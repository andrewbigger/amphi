﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amphi.Controllers;
using Amphi.Models;
using Amphi.Views;
using Amphi.Views.Settings;

namespace Amphi.Helpers
{
    public class ApplicationHelper
    {

        #region Import/Export
        public static void ExportConfig(Form parent)
        {
            try
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Title = "Export modes configuration";
                save.AddExtension = true;
                save.DefaultExt = "*.bin";
                save.Filter = "Bin Files (*.bin)|*.bin|All files (*.*)|*.*";
                save.ValidateNames = true;
                save.OverwritePrompt = true;

                DialogResult result = save.ShowDialog();

                if (result == DialogResult.OK)
                {
                    File.Copy(
                        ApplicationController.ModeDataPath,
                        save.FileName);

                    MessageBox.Show(
                        parent,
                        "Saved mode configuration to " + save.FileName,
                        "Success",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    parent,
                    "Cannot export your configuration because " + ex.Message,
                    "Save Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        public static void ImportConfig(Form parent)
        {
            try
            {
                OpenFileDialog open = new OpenFileDialog();
                open.CheckFileExists = true;
                open.Title = "Import modes configuration";
                open.Filter = "Bin Files (*.bin)|*.bin";

                DialogResult result = open.ShowDialog();

                if (result == DialogResult.OK)
                {
                    DialogResult confirm = MessageBox.Show(
                        parent,
                        "Please note this will overwrite your existing configuration. Is this okay?",
                        "Are you sure?",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Warning);

                    if (confirm == DialogResult.Yes)
                    {
                        File.Copy(
                            open.FileName,
                            ApplicationController.ModeDataPath,
                            true);

                        Application.Restart();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    parent,
                    "Cannot export your configuration because " + ex.Message,
                    "Save Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        #endregion
        #region Mode Transactions
        public static void AddMode(Form parent)
        {
            try
            {
                Mode stubMode = ApplicationController.CreateStubMode();
                ModeSettingsView create = new ModeSettingsView(stubMode, true);
                create.ShowDialog();

                if (stubMode.Name == "stub")
                {
                    ApplicationController.DestroyStubMode();
                }

                ApplicationController.Save();
            }
            catch (IOException ex)
            {
                MessageBox.Show(
                    parent,
                    "Cannot save your changes because " + ex.Message,
                    "Save Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    parent,
                    "Oops! A critical error has ocurred. Please contact support. \n \n" + ex.Message,
                    "Critical Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        public static void RemoveMode(Form parent, string mode)
        {
            try
            {
                DialogResult action = MessageBox.Show(
                    parent,
                    "Are you sure you want to delete " + mode + " and all of its Widgets?",
                    "Delete Widget",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);

                if (action == System.Windows.Forms.DialogResult.Yes)
                {
                    ApplicationController.Destroy(mode);
                    ApplicationController.Save();
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(
                    parent,
                    "Cannot save your changes because " + ex.Message,
                    "Save Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    parent,
                    "Oops! Something's gone wrong. Please contact support. \n \n" + ex.Message,
                    "Critical Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        public static void EditMode(Form parent, string mode)
        {
            try
            {
                Mode activeMode = ApplicationController.Modes.Find(mode)[0];
                ModeSettingsView edit = new ModeSettingsView(activeMode);
                edit.ShowDialog();
                ApplicationController.Save();
            }
            catch (IOException ex)
            {
                MessageBox.Show(
                    parent,
                    "Cannot save your changes because " + ex.Message,
                    "Save Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                parent,
                "Oops! Amphi has lost your mode. Please contact support. \n \n" + ex.Message,
                "Critical Error",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Widget Transactions
        public static void AddWidget(Form parent, string mode)
        {
            try
            {
                HtmlWidget stubWidget = ApplicationController.CreateStubWidget(mode);

                HtmlWidgetSettingsView create = new HtmlWidgetSettingsView(stubWidget, true);
                create.ShowDialog();
                if (stubWidget.Name == "stub" || stubWidget.GetUrl.ToString() == "http://stub.com")
                {
                    ApplicationController.DestroyStubWidget(mode);
                }
                else
                {
                    ApplicationController.WidgetViews.Create(create.ActiveWidget);
                    HtmlWidgetView view = ApplicationController.WidgetViews.Find(create.ActiveWidget)[0];
                    Mode targetMode = ApplicationController.Modes.Find(mode)[0];
                    if (ApplicationController.ActiveMode == targetMode)
                    {
                        view.Show();
                    }
                }

                ApplicationController.Save();
            }
            catch (IOException ex)
            {
                MessageBox.Show(
                    parent,
                    "Cannot save your changes because " + ex.Message,
                    "Save Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    parent,
                    "Oops! A critical error has ocurred. Please contact support. \n \n" + ex.Message,
                    "Critical Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        public static void EditWidget(Form parent, string mode, string widget)
        {
            try
            {
                HtmlWidget activeWidget = ApplicationController.Find(mode, widget);
                HtmlWidgetSettingsView edit = new HtmlWidgetSettingsView(activeWidget);
                edit.ShowDialog();
                ApplicationController.Save();
            }
            catch (IOException ex)
            {
                MessageBox.Show(
                    parent,
                    "Cannot save your changes because " + ex.Message,
                    "Save Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                parent,
                "Oops! Amphi has lost your widget. Please contact support. \n \n" + ex.Message,
                "Critical Error",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            }
        }

        public static void DeleteWidget(Form parent, string mode, string widget)
        {
            try
            {
                DialogResult action = MessageBox.Show(
                    parent,
                    "Are you sure you want to delete " + widget + "?",
                    "Delete Widget",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);

                if (action == System.Windows.Forms.DialogResult.Yes)
                {
                    ApplicationController.Destroy(mode, widget);
                    ApplicationController.Save();
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(
                    parent,
                    "Cannot save your changes because " + ex.Message,
                    "Save Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    parent,
                    "Oops! Something's gone wrong. Please contact support. \n \n" + ex.Message,
                    "Critical Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}
