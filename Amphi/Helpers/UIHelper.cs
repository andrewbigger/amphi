﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amphi.Controllers;
using Amphi.Models;
using Amphi.Views;
using MetroFramework;
using MetroFramework.Controls;
using MetroFramework.Forms;

namespace Amphi.Helpers
{
    public static class UIHelper
    {
        #region Static Properties
        public static int Top
        {
            get
            {
                // Height and end position of header
                return 62;
            }
        }

        public static MetroColorStyle Colour
        {
            get
            {
                return StyleOf(Properties.Settings.Default.Color);
            }
        }

        public static MetroThemeStyle Theme
        {
            get
            {
                return ThemeOf(Properties.Settings.Default.Theme);
            }
        }
        #endregion

        #region Application Style Helpers
        public static void SetTheme(MetroForm form)
        {
            form.Style = Colour;
            form.Theme = Theme;
        }

        public static void SetTheme(MetroButton button)
        {
            button.Style = Colour;
            button.Theme = Theme;
        }

        public static void SetTheme(MetroComboBox cmb)
        {
            cmb.Style = Colour;
            cmb.Theme = Theme;
        }

        public static void SetTheme(MetroLabel lbl)
        {
            lbl.Style = Colour;
            lbl.Theme = Theme;
        }

        public static void SetTheme(MetroTextBox txt)
        {
            txt.Style = Colour;
            txt.Theme = Theme;
        }

        public static void SetTheme(MetroTabControl tabs)
        {
            tabs.Style = Colour;
            tabs.Theme = Theme;
        }

        public static void SetTheme(MetroTabPage tab)
        {
            tab.Style = Colour;
            tab.Theme = Theme;
        }

        public static void SetTheme(MetroTile tile)
        {
            tile.Style = Colour;
            tile.Theme = Theme;
        }

        public static void SetTheme(MetroProgressBar bar)
        {
            bar.Style = Colour;
            bar.Theme = Theme;
        }

        public static void SetTheme(MetroPanel pnl)
        {
            pnl.Style = Colour;
            pnl.Theme = Theme;
        }

        public static MetroColorStyle StyleOf(string color)
        {
            MetroColorStyle style = MetroColorStyle.Blue;
            switch (color)
            {
                case "Black":
                    style = MetroColorStyle.Black;
                    break;
                case "Green":
                    style = MetroColorStyle.Green;
                    break;
                case "Blue":
                    style = MetroColorStyle.Blue;
                    break;
                case "Orange":
                    style = MetroColorStyle.Orange;
                    break;
                case "Brown":
                    style = MetroColorStyle.Brown;
                    break;
                case "Lime":
                    style = MetroColorStyle.Lime;
                    break;
                case "Magenta":
                    style = MetroColorStyle.Magenta;
                    break;
                case "Pink":
                    style = MetroColorStyle.Pink;
                    break;
                case "Purple":
                    style = MetroColorStyle.Purple;
                    break;
                case "Red":
                    style = MetroColorStyle.Red;
                    break;
                case "Silver":
                    style = MetroColorStyle.Silver;
                    break;
                case "Teal":
                    style = MetroColorStyle.Teal;
                    break;
                case "White":
                    style = MetroColorStyle.White;
                    break;
                case "Yellow":
                    style = MetroColorStyle.Yellow;
                    break;
                default:
                    style = MetroColorStyle.Blue;
                break;
            }

            return style;
        }

        public static MetroThemeStyle ThemeOf(string shade)
        {
            if (shade == "Dark") 
            { 
                return MetroThemeStyle.Dark; 
            }

            return MetroThemeStyle.Light;
        }
        #endregion
        
        #region Header Helpers
        public static void PositionHeader(MetroForm header)
        {
            header.StartPosition = FormStartPosition.Manual;
            header.Width = Screen.PrimaryScreen.Bounds.Width;
            header.Location = new Point(0, 0);
            header.Height = Top;
        }
        #endregion

        #region Settings Helpers
        public static void PopulateWidgetList(MetroComboBox cmb, Mode mode)
        {
            cmb.Items.Clear();
            foreach (HtmlWidget w in mode.Widgets.Index)
            {
                cmb.Items.Add(w.Name);
            }
        }
        #endregion

        #region Mode Helpers
        public static void ShowActiveMode()
        {
            HideAll();
            foreach (HtmlWidget w in ApplicationController.ActiveMode.Widgets.Index)
            {
                ApplicationController.WidgetViews.Create(w);
            }

            foreach (HtmlWidgetView v in ApplicationController.WidgetViews.Find(ApplicationController.ActiveMode))
            {
                v.Show();
            }
        }

        public static void HideAll()
        {
            foreach (HtmlWidgetView v in ApplicationController.WidgetViews.Index)
            {
                v.Hide();
            }
        }

        #endregion

        #region Generic Helpers
        public static void SetSelectedItem(MetroComboBox cmb, string selection)
        {
            foreach (object item in cmb.Items)
            {
                if (item.ToString() == selection)
                {
                    cmb.SelectedItem = item;
                }
            }
        }
        #endregion

        #region Widget Helpers

        #region Form Modes
        public static void ToggleLock(Form form, bool alreadyLocked, bool adjustSize = true)
        {
            int height = form.Size.Height;
            int width = form.Size.Width;

            if (alreadyLocked == true)
            {
                form.FormBorderStyle = FormBorderStyle.SizableToolWindow;
            }
            else
            {
                form.FormBorderStyle = FormBorderStyle.None;
            }

            if (adjustSize)
            {
                SizeWindow(form, new Size(width, height));
            }
        }

        public static void ToggleFullScreen(Form form, bool alreadyFullScreen)
        {
            if (alreadyFullScreen == true)
            {
                form.WindowState = FormWindowState.Normal;
                ToggleLock(form, true, false);
            }
            else
            {
                form.WindowState = FormWindowState.Maximized;
                ToggleLock(form, false, false);
            }
        }
        #endregion

        #region Form Icon Helpers
        public static int ToggleIconSelection(bool state)
        {
            if (state == true)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        #endregion

        #region Form Size Helpers
        public static Size WindowBounds(Screen targetScreen, int widthFactor = 2, int heightFactor = 2)
        {
            Size result = new Size();
            result.Width = targetScreen.Bounds.Width / widthFactor;
            if (targetScreen.Primary)
            {
                result.Height = (targetScreen.Bounds.Height - Top) / heightFactor;
            }
            else
            {
                result.Height = targetScreen.Bounds.Height / heightFactor;
            }

            return result;
        }

        public static void SizeWindow(Form form, Size bounds)
        {
            form.Size = bounds;
        }
        #endregion

        #region Form Location Helpers
        public static Screen WindowScreen(Form form)
        {
            return Screen.FromControl(form);
        }

        public static Point WindowLocation(Screen targetScreen, Size formSize, string verticalAlignment = "top", string horizontalAlignment = "left")
        {
            int x = HorizontalPoint(targetScreen, formSize, horizontalAlignment);
            int y = VerticalPoint(targetScreen, formSize, verticalAlignment);
        
            return new Point(x, y);
        }

        public static void PositionWindow(Form form, Point location, bool offset = true)
        {
            Screen targetScreen = Screen.FromControl(form);
            if (!targetScreen.Primary && offset == true)
            {
                location.Offset(targetScreen.Bounds.Left, targetScreen.Bounds.Top);
            }

            form.Location = location;
        }

        private static int HorizontalPoint(Screen targetScreen, Size formSize, string horizontalAlignment)
        {
            int x;
            switch (horizontalAlignment)
            {
                case "centre":
                    x = (targetScreen.Bounds.Width / 2) - (formSize.Width / 2);
                    break;
                case "right":
                    x = targetScreen.Bounds.Width - formSize.Width;
                    break;
                default: 
                    x = 0;
                    break;
            }

            return x;
        }

        private static int VerticalPoint(Screen targetScreen, Size formSize, string verticalAlignment)
        {
            int y;
            switch (verticalAlignment)
            {
                case "bottom":
                    y = HeightOf(targetScreen) - formSize.Height;
                    break;
                default:
                    y = TopOf(targetScreen);
                    break;
            }

            return y;
        }

        private static int HeightOf(Screen targetScreen)
        {
            if (targetScreen.Primary)
            {
                return targetScreen.Bounds.Height - Top;
            }
            else
            {
                return targetScreen.Bounds.Height;
            }
        }

        private static int TopOf(Screen targetScreen)
        {
            if (targetScreen.Primary)
            {
               return Top;
            }
            else
            {
                return 0;
            }
        }
        #endregion
       
        #endregion
    }
}