﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amphi.Helpers
{
    #region Invalid Widget Name Exception
    public class InvalidWidgetNameException : Exception
    {
        public InvalidWidgetNameException()
        {
        }

        public InvalidWidgetNameException(string message)
            : base(message)
        {
        }

        public InvalidWidgetNameException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    #endregion

    #region Invalid Widget URL Exception
    public class InvalidWidgetUrlException : Exception
    {
        public InvalidWidgetUrlException()
        {
        }

        public InvalidWidgetUrlException(string message)
            : base(message)
        {
        }

        public InvalidWidgetUrlException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    #endregion

    #region Invalid Mode Name Exception
    public class InvalidModeNameException : Exception
    {
        public InvalidModeNameException()
        {
        }

        public InvalidModeNameException(string message)
            : base(message)
        {
        }

        public InvalidModeNameException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    #endregion

    #region Missing Item Exception
    public class MissingItemException : Exception
    {
        public MissingItemException()
        {
        }

        public MissingItemException(string message)
            : base(message)
        {
        }

        public MissingItemException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    #endregion
}