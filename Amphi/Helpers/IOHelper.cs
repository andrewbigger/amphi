﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Amphi.Helpers
{
    public static class IOHelper
    {
        private static StreamWriter fw;
        private static FileStream fr;

        #region Public Methods
        public static void Save(string path, object obj)
        {
            try
            {
                MemoryStream streamOfObject = SerializeBinary(obj);
                Write(path, streamOfObject);
            }
            catch (DirectoryNotFoundException)
            {
                TryCreateDirectory(path);
            }
            catch (IOException e)
            {
                throw e;
            }
        }

        public static object Load(string path)
        {
            try
            {
                MemoryStream streamOfObject = Read(path);
                object obj = DeserializeBinary(streamOfObject);
                return obj;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void Write(string path, MemoryStream content)
        {
            try
            {
                fw = new StreamWriter(path);
                content.WriteTo(fw.BaseStream);
                content.Close();
                fw.Close();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static MemoryStream Read(string path)
        {
            MemoryStream content;
            try
            {
                fr = new FileStream(path, FileMode.Open);
                content = new MemoryStream();
                content.SetLength(fr.Length);

                byte[] bytes = new byte[fr.Length];
                int numBytesToRead = (int)fr.Length;
                int numBytesRead = 0;

                while (numBytesToRead > 0)
                {
                    int n = fr.Read(content.GetBuffer(), numBytesRead, numBytesToRead);

                    if (n == 0)
                    {
                        break;
                    }

                    numBytesRead += n;
                    numBytesToRead -= n;
                }

                fr.Close();
            }
            catch (Exception e)
            {
                throw e;
            }

            return content;
        }

        public static MemoryStream SerializeBinary(object obj)
        {
            try
            {
                BinaryFormatter serializer = new BinaryFormatter();
                MemoryStream content = new MemoryStream();
                serializer.Serialize(content, obj);
                return content;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static object DeserializeBinary(MemoryStream content)
        {
            try
            {
                content.Position = 0;
                BinaryFormatter deserializer = new BinaryFormatter();
                return deserializer.Deserialize(content);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Private Methods
        private static void TryCreateDirectory(string path)
        {
            try
            {
                FileInfo fi = new FileInfo(path);
                Directory.CreateDirectory(fi.DirectoryName);
            }
            catch (IOException e)
            {
                throw e;
            }
        }
        #endregion
    }
}
