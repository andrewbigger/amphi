﻿namespace Amphi.Views.Settings
{
    partial class HtmlWidgetSettingsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HtmlWidgetSettingsView));
            this.SaveButton = new MetroFramework.Controls.MetroButton();
            this.WidgetNameLabel = new MetroFramework.Controls.MetroLabel();
            this.WidgetNameTextBox = new MetroFramework.Controls.MetroTextBox();
            this.WidgetUrlTextBox = new MetroFramework.Controls.MetroTextBox();
            this.WidgetUrlLabel = new MetroFramework.Controls.MetroLabel();
            this.BottomLeftButton = new MetroFramework.Controls.MetroTile();
            this.BottomCentreButton = new MetroFramework.Controls.MetroTile();
            this.BottomRightButton = new MetroFramework.Controls.MetroTile();
            this.LeftHalfButton = new MetroFramework.Controls.MetroTile();
            this.RightHalfButton = new MetroFramework.Controls.MetroTile();
            this.TopLeftButton = new MetroFramework.Controls.MetroTile();
            this.CentreButton = new MetroFramework.Controls.MetroTile();
            this.TopCentreButton = new MetroFramework.Controls.MetroTile();
            this.TopRightButton = new MetroFramework.Controls.MetroTile();
            this.ScreenPositionLabel = new MetroFramework.Controls.MetroLabel();
            this.CentreLeftButton = new MetroFramework.Controls.MetroTile();
            this.CentreThirdButton = new MetroFramework.Controls.MetroTile();
            this.CentreRightButton = new MetroFramework.Controls.MetroTile();
            this.CancelHtmlWidgetEditButton = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveButton.Highlight = true;
            this.SaveButton.Location = new System.Drawing.Point(530, 389);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(127, 37);
            this.SaveButton.TabIndex = 7;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseSelectable = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // WidgetNameLabel
            // 
            this.WidgetNameLabel.AutoSize = true;
            this.WidgetNameLabel.Location = new System.Drawing.Point(23, 93);
            this.WidgetNameLabel.Margin = new System.Windows.Forms.Padding(3);
            this.WidgetNameLabel.Name = "WidgetNameLabel";
            this.WidgetNameLabel.Size = new System.Drawing.Size(45, 19);
            this.WidgetNameLabel.TabIndex = 9;
            this.WidgetNameLabel.Text = "Name";
            // 
            // WidgetNameTextBox
            // 
            this.WidgetNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WidgetNameTextBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.WidgetNameTextBox.Lines = new string[] {
        "Normal Textbox"};
            this.WidgetNameTextBox.Location = new System.Drawing.Point(74, 93);
            this.WidgetNameTextBox.MaxLength = 32767;
            this.WidgetNameTextBox.Name = "WidgetNameTextBox";
            this.WidgetNameTextBox.PasswordChar = '\0';
            this.WidgetNameTextBox.PromptText = "Widget Name";
            this.WidgetNameTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.WidgetNameTextBox.SelectedText = "";
            this.WidgetNameTextBox.Size = new System.Drawing.Size(435, 22);
            this.WidgetNameTextBox.TabIndex = 11;
            this.WidgetNameTextBox.Text = "Normal Textbox";
            this.WidgetNameTextBox.UseSelectable = true;
            // 
            // WidgetUrlTextBox
            // 
            this.WidgetUrlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WidgetUrlTextBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.WidgetUrlTextBox.Lines = new string[] {
        "Normal Textbox"};
            this.WidgetUrlTextBox.Location = new System.Drawing.Point(74, 121);
            this.WidgetUrlTextBox.MaxLength = 32767;
            this.WidgetUrlTextBox.Name = "WidgetUrlTextBox";
            this.WidgetUrlTextBox.PasswordChar = '\0';
            this.WidgetUrlTextBox.PromptText = "http://www.some-widget.com";
            this.WidgetUrlTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.WidgetUrlTextBox.SelectedText = "";
            this.WidgetUrlTextBox.Size = new System.Drawing.Size(435, 22);
            this.WidgetUrlTextBox.TabIndex = 13;
            this.WidgetUrlTextBox.Text = "Normal Textbox";
            this.WidgetUrlTextBox.UseSelectable = true;
            // 
            // WidgetUrlLabel
            // 
            this.WidgetUrlLabel.AutoSize = true;
            this.WidgetUrlLabel.Location = new System.Drawing.Point(23, 121);
            this.WidgetUrlLabel.Margin = new System.Windows.Forms.Padding(3);
            this.WidgetUrlLabel.Name = "WidgetUrlLabel";
            this.WidgetUrlLabel.Size = new System.Drawing.Size(32, 19);
            this.WidgetUrlLabel.TabIndex = 12;
            this.WidgetUrlLabel.Text = "URL";
            // 
            // BottomLeftButton
            // 
            this.BottomLeftButton.ActiveControl = null;
            this.BottomLeftButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BottomLeftButton.Location = new System.Drawing.Point(522, 311);
            this.BottomLeftButton.Name = "BottomLeftButton";
            this.BottomLeftButton.Size = new System.Drawing.Size(41, 42);
            this.BottomLeftButton.TabIndex = 60;
            this.BottomLeftButton.Text = "*";
            this.BottomLeftButton.UseSelectable = true;
            this.BottomLeftButton.Click += new System.EventHandler(this.BottomLeftButton_Click);
            // 
            // BottomCentreButton
            // 
            this.BottomCentreButton.ActiveControl = null;
            this.BottomCentreButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BottomCentreButton.Location = new System.Drawing.Point(569, 311);
            this.BottomCentreButton.Name = "BottomCentreButton";
            this.BottomCentreButton.Size = new System.Drawing.Size(41, 42);
            this.BottomCentreButton.TabIndex = 59;
            this.BottomCentreButton.Text = "*";
            this.BottomCentreButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BottomCentreButton.UseSelectable = true;
            this.BottomCentreButton.Click += new System.EventHandler(this.BottomCentreButton_Click);
            // 
            // BottomRightButton
            // 
            this.BottomRightButton.ActiveControl = null;
            this.BottomRightButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BottomRightButton.Location = new System.Drawing.Point(616, 311);
            this.BottomRightButton.Name = "BottomRightButton";
            this.BottomRightButton.Size = new System.Drawing.Size(41, 42);
            this.BottomRightButton.TabIndex = 58;
            this.BottomRightButton.Text = "*";
            this.BottomRightButton.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.BottomRightButton.UseSelectable = true;
            this.BottomRightButton.Click += new System.EventHandler(this.BottomRightButton_Click);
            // 
            // LeftHalfButton
            // 
            this.LeftHalfButton.ActiveControl = null;
            this.LeftHalfButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LeftHalfButton.Location = new System.Drawing.Point(522, 141);
            this.LeftHalfButton.Name = "LeftHalfButton";
            this.LeftHalfButton.Size = new System.Drawing.Size(65, 42);
            this.LeftHalfButton.TabIndex = 57;
            this.LeftHalfButton.Text = "Left";
            this.LeftHalfButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LeftHalfButton.UseSelectable = true;
            this.LeftHalfButton.Click += new System.EventHandler(this.LeftHalfButton_Click);
            // 
            // RightHalfButton
            // 
            this.RightHalfButton.ActiveControl = null;
            this.RightHalfButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RightHalfButton.Location = new System.Drawing.Point(592, 141);
            this.RightHalfButton.Name = "RightHalfButton";
            this.RightHalfButton.Size = new System.Drawing.Size(65, 42);
            this.RightHalfButton.TabIndex = 56;
            this.RightHalfButton.Text = "Right";
            this.RightHalfButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.RightHalfButton.UseSelectable = true;
            this.RightHalfButton.Click += new System.EventHandler(this.RightHalfButton_Click);
            // 
            // TopLeftButton
            // 
            this.TopLeftButton.ActiveControl = null;
            this.TopLeftButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TopLeftButton.Location = new System.Drawing.Point(522, 93);
            this.TopLeftButton.Name = "TopLeftButton";
            this.TopLeftButton.Size = new System.Drawing.Size(41, 42);
            this.TopLeftButton.TabIndex = 55;
            this.TopLeftButton.Text = "*";
            this.TopLeftButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.TopLeftButton.UseSelectable = true;
            this.TopLeftButton.Click += new System.EventHandler(this.TopLeftButton_Click);
            // 
            // CentreButton
            // 
            this.CentreButton.ActiveControl = null;
            this.CentreButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CentreButton.Location = new System.Drawing.Point(522, 189);
            this.CentreButton.Name = "CentreButton";
            this.CentreButton.Size = new System.Drawing.Size(135, 49);
            this.CentreButton.TabIndex = 52;
            this.CentreButton.Text = "Centre Screen";
            this.CentreButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CentreButton.UseSelectable = true;
            this.CentreButton.Click += new System.EventHandler(this.CentreButton_Click);
            // 
            // TopCentreButton
            // 
            this.TopCentreButton.ActiveControl = null;
            this.TopCentreButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TopCentreButton.Location = new System.Drawing.Point(569, 93);
            this.TopCentreButton.Name = "TopCentreButton";
            this.TopCentreButton.Size = new System.Drawing.Size(41, 42);
            this.TopCentreButton.TabIndex = 54;
            this.TopCentreButton.Text = "*";
            this.TopCentreButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.TopCentreButton.UseSelectable = true;
            this.TopCentreButton.Click += new System.EventHandler(this.TopCentreButton_Click);
            // 
            // TopRightButton
            // 
            this.TopRightButton.ActiveControl = null;
            this.TopRightButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TopRightButton.Location = new System.Drawing.Point(616, 93);
            this.TopRightButton.Name = "TopRightButton";
            this.TopRightButton.Size = new System.Drawing.Size(41, 42);
            this.TopRightButton.TabIndex = 53;
            this.TopRightButton.Text = "*";
            this.TopRightButton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.TopRightButton.UseSelectable = true;
            this.TopRightButton.Click += new System.EventHandler(this.TopRightButton_Click);
            // 
            // ScreenPositionLabel
            // 
            this.ScreenPositionLabel.AutoSize = true;
            this.ScreenPositionLabel.Location = new System.Drawing.Point(560, 68);
            this.ScreenPositionLabel.Margin = new System.Windows.Forms.Padding(3);
            this.ScreenPositionLabel.Name = "ScreenPositionLabel";
            this.ScreenPositionLabel.Size = new System.Drawing.Size(97, 19);
            this.ScreenPositionLabel.TabIndex = 51;
            this.ScreenPositionLabel.Text = "Screen Position";
            // 
            // CentreLeftButton
            // 
            this.CentreLeftButton.ActiveControl = null;
            this.CentreLeftButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CentreLeftButton.Location = new System.Drawing.Point(522, 244);
            this.CentreLeftButton.Name = "CentreLeftButton";
            this.CentreLeftButton.Size = new System.Drawing.Size(41, 61);
            this.CentreLeftButton.TabIndex = 63;
            this.CentreLeftButton.Text = "*";
            this.CentreLeftButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CentreLeftButton.UseSelectable = true;
            this.CentreLeftButton.Click += new System.EventHandler(this.CentreLeftButton_Click);
            // 
            // CentreThirdButton
            // 
            this.CentreThirdButton.ActiveControl = null;
            this.CentreThirdButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CentreThirdButton.Location = new System.Drawing.Point(569, 244);
            this.CentreThirdButton.Name = "CentreThirdButton";
            this.CentreThirdButton.Size = new System.Drawing.Size(41, 61);
            this.CentreThirdButton.TabIndex = 62;
            this.CentreThirdButton.Text = "*";
            this.CentreThirdButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CentreThirdButton.UseSelectable = true;
            this.CentreThirdButton.Click += new System.EventHandler(this.CentreThirdButton_Click);
            // 
            // CentreRightButton
            // 
            this.CentreRightButton.ActiveControl = null;
            this.CentreRightButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CentreRightButton.Location = new System.Drawing.Point(616, 244);
            this.CentreRightButton.Name = "CentreRightButton";
            this.CentreRightButton.Size = new System.Drawing.Size(41, 61);
            this.CentreRightButton.TabIndex = 61;
            this.CentreRightButton.Text = "*";
            this.CentreRightButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CentreRightButton.UseSelectable = true;
            this.CentreRightButton.Click += new System.EventHandler(this.CentreRightButton_Click);
            // 
            // CancelHtmlWidgetEditButton
            // 
            this.CancelHtmlWidgetEditButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CancelHtmlWidgetEditButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelHtmlWidgetEditButton.Location = new System.Drawing.Point(397, 389);
            this.CancelHtmlWidgetEditButton.Name = "CancelHtmlWidgetEditButton";
            this.CancelHtmlWidgetEditButton.Size = new System.Drawing.Size(127, 37);
            this.CancelHtmlWidgetEditButton.TabIndex = 64;
            this.CancelHtmlWidgetEditButton.Text = "Cancel";
            this.CancelHtmlWidgetEditButton.UseSelectable = true;
            this.CancelHtmlWidgetEditButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // HtmlWidgetSettingsView
            // 
            this.AcceptButton = this.SaveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 449);
            this.Controls.Add(this.CancelHtmlWidgetEditButton);
            this.Controls.Add(this.CentreLeftButton);
            this.Controls.Add(this.CentreThirdButton);
            this.Controls.Add(this.CentreRightButton);
            this.Controls.Add(this.BottomLeftButton);
            this.Controls.Add(this.BottomCentreButton);
            this.Controls.Add(this.BottomRightButton);
            this.Controls.Add(this.LeftHalfButton);
            this.Controls.Add(this.RightHalfButton);
            this.Controls.Add(this.TopLeftButton);
            this.Controls.Add(this.CentreButton);
            this.Controls.Add(this.TopCentreButton);
            this.Controls.Add(this.TopRightButton);
            this.Controls.Add(this.ScreenPositionLabel);
            this.Controls.Add(this.WidgetUrlTextBox);
            this.Controls.Add(this.WidgetUrlLabel);
            this.Controls.Add(this.WidgetNameTextBox);
            this.Controls.Add(this.WidgetNameLabel);
            this.Controls.Add(this.SaveButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HtmlWidgetSettingsView";
            this.Text = "HtmlWidget Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton SaveButton;
        private MetroFramework.Controls.MetroLabel WidgetNameLabel;
        private MetroFramework.Controls.MetroTextBox WidgetNameTextBox;
        private MetroFramework.Controls.MetroTextBox WidgetUrlTextBox;
        private MetroFramework.Controls.MetroLabel WidgetUrlLabel;
        private MetroFramework.Controls.MetroTile BottomLeftButton;
        private MetroFramework.Controls.MetroTile BottomCentreButton;
        private MetroFramework.Controls.MetroTile BottomRightButton;
        private MetroFramework.Controls.MetroTile LeftHalfButton;
        private MetroFramework.Controls.MetroTile RightHalfButton;
        private MetroFramework.Controls.MetroTile TopLeftButton;
        private MetroFramework.Controls.MetroTile CentreButton;
        private MetroFramework.Controls.MetroTile TopCentreButton;
        private MetroFramework.Controls.MetroTile TopRightButton;
        private MetroFramework.Controls.MetroLabel ScreenPositionLabel;
        private MetroFramework.Controls.MetroTile CentreLeftButton;
        private MetroFramework.Controls.MetroTile CentreThirdButton;
        private MetroFramework.Controls.MetroTile CentreRightButton;
        private MetroFramework.Controls.MetroButton CancelHtmlWidgetEditButton;
    }
}