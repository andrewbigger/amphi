﻿namespace Amphi.Views.Settings
{
    partial class ModeSettingsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModeSettingsView));
            this.SaveButton = new MetroFramework.Controls.MetroButton();
            this.ModeNameLabel = new MetroFramework.Controls.MetroLabel();
            this.ModeNameTextBox = new MetroFramework.Controls.MetroTextBox();
            this.EditWidgetButton = new MetroFramework.Controls.MetroButton();
            this.RemoveWidgetButton = new MetroFramework.Controls.MetroButton();
            this.AddWidgetButton = new MetroFramework.Controls.MetroButton();
            this.WidgetsLabel = new MetroFramework.Controls.MetroLabel();
            this.WidgetListComboBox = new MetroFramework.Controls.MetroComboBox();
            this.CancelModeEditButton = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveButton.Highlight = true;
            this.SaveButton.Location = new System.Drawing.Point(530, 389);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(127, 37);
            this.SaveButton.TabIndex = 7;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseSelectable = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ModeNameLabel
            // 
            this.ModeNameLabel.AutoSize = true;
            this.ModeNameLabel.Location = new System.Drawing.Point(23, 93);
            this.ModeNameLabel.Margin = new System.Windows.Forms.Padding(3);
            this.ModeNameLabel.Name = "ModeNameLabel";
            this.ModeNameLabel.Size = new System.Drawing.Size(45, 19);
            this.ModeNameLabel.TabIndex = 9;
            this.ModeNameLabel.Text = "Name";
            // 
            // ModeNameTextBox
            // 
            this.ModeNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ModeNameTextBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ModeNameTextBox.Lines = new string[] {
        "Normal Textbox"};
            this.ModeNameTextBox.Location = new System.Drawing.Point(92, 93);
            this.ModeNameTextBox.MaxLength = 32767;
            this.ModeNameTextBox.Name = "ModeNameTextBox";
            this.ModeNameTextBox.PasswordChar = '\0';
            this.ModeNameTextBox.PromptText = "Mode Name";
            this.ModeNameTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ModeNameTextBox.SelectedText = "";
            this.ModeNameTextBox.Size = new System.Drawing.Size(565, 22);
            this.ModeNameTextBox.TabIndex = 11;
            this.ModeNameTextBox.Text = "Normal Textbox";
            this.ModeNameTextBox.UseSelectable = true;
            // 
            // EditWidgetButton
            // 
            this.EditWidgetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EditWidgetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EditWidgetButton.Location = new System.Drawing.Point(471, 156);
            this.EditWidgetButton.Name = "EditWidgetButton";
            this.EditWidgetButton.Size = new System.Drawing.Size(186, 31);
            this.EditWidgetButton.TabIndex = 42;
            this.EditWidgetButton.Text = "Edit Selected";
            this.EditWidgetButton.UseSelectable = true;
            this.EditWidgetButton.Click += new System.EventHandler(this.EditWidgetButton_Click);
            // 
            // RemoveWidgetButton
            // 
            this.RemoveWidgetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RemoveWidgetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RemoveWidgetButton.Location = new System.Drawing.Point(279, 156);
            this.RemoveWidgetButton.Name = "RemoveWidgetButton";
            this.RemoveWidgetButton.Size = new System.Drawing.Size(186, 31);
            this.RemoveWidgetButton.TabIndex = 41;
            this.RemoveWidgetButton.Text = "Remove Selected";
            this.RemoveWidgetButton.UseSelectable = true;
            this.RemoveWidgetButton.Click += new System.EventHandler(this.RemoveWidgetButton_Click);
            // 
            // AddWidgetButton
            // 
            this.AddWidgetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddWidgetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddWidgetButton.Location = new System.Drawing.Point(92, 156);
            this.AddWidgetButton.Name = "AddWidgetButton";
            this.AddWidgetButton.Size = new System.Drawing.Size(181, 31);
            this.AddWidgetButton.TabIndex = 40;
            this.AddWidgetButton.Text = "Add New";
            this.AddWidgetButton.UseSelectable = true;
            this.AddWidgetButton.Click += new System.EventHandler(this.AddWidgetButton_Click);
            // 
            // WidgetsLabel
            // 
            this.WidgetsLabel.AutoSize = true;
            this.WidgetsLabel.Location = new System.Drawing.Point(23, 121);
            this.WidgetsLabel.Name = "WidgetsLabel";
            this.WidgetsLabel.Size = new System.Drawing.Size(57, 19);
            this.WidgetsLabel.TabIndex = 38;
            this.WidgetsLabel.Text = "Widgets";
            // 
            // WidgetListComboBox
            // 
            this.WidgetListComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Amphi.Properties.Settings.Default, "Color", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.WidgetListComboBox.FormattingEnabled = true;
            this.WidgetListComboBox.ItemHeight = 23;
            this.WidgetListComboBox.Items.AddRange(new object[] {
            "Blue",
            "Orange",
            "Red",
            "Green",
            "Black",
            "Brown",
            "Lime",
            "Magenta",
            "Pink",
            "Purple",
            "Silver",
            "Teal",
            "White",
            "Yellow"});
            this.WidgetListComboBox.Location = new System.Drawing.Point(92, 121);
            this.WidgetListComboBox.Name = "WidgetListComboBox";
            this.WidgetListComboBox.Size = new System.Drawing.Size(565, 29);
            this.WidgetListComboBox.TabIndex = 39;
            this.WidgetListComboBox.Text = global::Amphi.Properties.Settings.Default.Color;
            this.WidgetListComboBox.UseSelectable = true;
            // 
            // CancelModeEditButton
            // 
            this.CancelModeEditButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CancelModeEditButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelModeEditButton.Location = new System.Drawing.Point(397, 389);
            this.CancelModeEditButton.Name = "CancelModeEditButton";
            this.CancelModeEditButton.Size = new System.Drawing.Size(127, 37);
            this.CancelModeEditButton.TabIndex = 65;
            this.CancelModeEditButton.Text = "Cancel";
            this.CancelModeEditButton.UseSelectable = true;
            this.CancelModeEditButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ModeSettingsView
            // 
            this.AcceptButton = this.SaveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 449);
            this.Controls.Add(this.CancelModeEditButton);
            this.Controls.Add(this.EditWidgetButton);
            this.Controls.Add(this.RemoveWidgetButton);
            this.Controls.Add(this.AddWidgetButton);
            this.Controls.Add(this.WidgetListComboBox);
            this.Controls.Add(this.WidgetsLabel);
            this.Controls.Add(this.ModeNameTextBox);
            this.Controls.Add(this.ModeNameLabel);
            this.Controls.Add(this.SaveButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ModeSettingsView";
            this.Text = "Mode Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton SaveButton;
        private MetroFramework.Controls.MetroLabel ModeNameLabel;
        private MetroFramework.Controls.MetroTextBox ModeNameTextBox;
        private MetroFramework.Controls.MetroButton EditWidgetButton;
        private MetroFramework.Controls.MetroButton RemoveWidgetButton;
        private MetroFramework.Controls.MetroButton AddWidgetButton;
        private MetroFramework.Controls.MetroComboBox WidgetListComboBox;
        private MetroFramework.Controls.MetroLabel WidgetsLabel;
        private MetroFramework.Controls.MetroButton CancelModeEditButton;
    }
}