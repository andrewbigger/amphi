﻿using System;

namespace Amphi.Views
{
    partial class HtmlWidgetView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Hide form from view
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            this.Hide();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HtmlWidgetView));
            this.LockImages = new System.Windows.Forms.ImageList(this.components);
            this.ResizeImages = new System.Windows.Forms.ImageList(this.components);
            this.HtmlWidgetSplit = new System.Windows.Forms.SplitContainer();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BrowserPanel = new System.Windows.Forms.Panel();
            this.BrowserControlPanel = new System.Windows.Forms.Panel();
            this.GoButton = new System.Windows.Forms.Button();
            this.ToggleLockButton = new System.Windows.Forms.Button();
            this.UrlTextBox = new System.Windows.Forms.TextBox();
            this.ToggleFullscreenButton = new System.Windows.Forms.Button();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.EditButton = new System.Windows.Forms.Button();
            this.BrowserStatusBar = new System.Windows.Forms.ProgressBar();
            this.ToggleWidgetSettingsButton = new System.Windows.Forms.Button();
            this.SettingsImages = new System.Windows.Forms.ImageList(this.components);
            this.WidgetBrowser = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.HtmlWidgetSplit)).BeginInit();
            this.HtmlWidgetSplit.Panel1.SuspendLayout();
            this.HtmlWidgetSplit.Panel2.SuspendLayout();
            this.HtmlWidgetSplit.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.BrowserPanel.SuspendLayout();
            this.BrowserControlPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LockImages
            // 
            this.LockImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("LockImages.ImageStream")));
            this.LockImages.TransparentColor = System.Drawing.Color.Transparent;
            this.LockImages.Images.SetKeyName(0, "icon_unlock.png");
            this.LockImages.Images.SetKeyName(1, "icon_lock.png");
            // 
            // ResizeImages
            // 
            this.ResizeImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ResizeImages.ImageStream")));
            this.ResizeImages.TransparentColor = System.Drawing.Color.Transparent;
            this.ResizeImages.Images.SetKeyName(0, "icon_fullscreen.png");
            this.ResizeImages.Images.SetKeyName(1, "icon_exit_fullscreen.png");
            // 
            // HtmlWidgetSplit
            // 
            this.HtmlWidgetSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HtmlWidgetSplit.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.HtmlWidgetSplit.IsSplitterFixed = true;
            this.HtmlWidgetSplit.Location = new System.Drawing.Point(0, 0);
            this.HtmlWidgetSplit.Name = "HtmlWidgetSplit";
            this.HtmlWidgetSplit.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // HtmlWidgetSplit.Panel1
            // 
            this.HtmlWidgetSplit.Panel1.Controls.Add(this.ControlPanel);
            // 
            // HtmlWidgetSplit.Panel2
            // 
            this.HtmlWidgetSplit.Panel2.Controls.Add(this.BrowserStatusBar);
            this.HtmlWidgetSplit.Panel2.Controls.Add(this.ToggleWidgetSettingsButton);
            this.HtmlWidgetSplit.Panel2.Controls.Add(this.WidgetBrowser);
            this.HtmlWidgetSplit.Size = new System.Drawing.Size(372, 295);
            this.HtmlWidgetSplit.SplitterDistance = 38;
            this.HtmlWidgetSplit.TabIndex = 0;
            // 
            // ControlPanel
            // 
            this.ControlPanel.Controls.Add(this.BrowserPanel);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ControlPanel.Location = new System.Drawing.Point(0, 0);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(372, 38);
            this.ControlPanel.TabIndex = 6;
            // 
            // BrowserPanel
            // 
            this.BrowserPanel.Controls.Add(this.BrowserControlPanel);
            this.BrowserPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BrowserPanel.Location = new System.Drawing.Point(0, 0);
            this.BrowserPanel.Name = "BrowserPanel";
            this.BrowserPanel.Size = new System.Drawing.Size(372, 38);
            this.BrowserPanel.TabIndex = 0;
            // 
            // BrowserControlPanel
            // 
            this.BrowserControlPanel.Controls.Add(this.GoButton);
            this.BrowserControlPanel.Controls.Add(this.ToggleLockButton);
            this.BrowserControlPanel.Controls.Add(this.UrlTextBox);
            this.BrowserControlPanel.Controls.Add(this.ToggleFullscreenButton);
            this.BrowserControlPanel.Controls.Add(this.RefreshButton);
            this.BrowserControlPanel.Controls.Add(this.EditButton);
            this.BrowserControlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BrowserControlPanel.Location = new System.Drawing.Point(0, 0);
            this.BrowserControlPanel.Name = "BrowserControlPanel";
            this.BrowserControlPanel.Size = new System.Drawing.Size(372, 38);
            this.BrowserControlPanel.TabIndex = 8;
            // 
            // GoButton
            // 
            this.GoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GoButton.Image = global::Amphi.Properties.Resources.icon_right_arrow;
            this.GoButton.Location = new System.Drawing.Point(334, 6);
            this.GoButton.Name = "GoButton";
            this.GoButton.Size = new System.Drawing.Size(26, 26);
            this.GoButton.TabIndex = 6;
            this.GoButton.UseVisualStyleBackColor = true;
            this.GoButton.Click += new System.EventHandler(this.GoButton_Click);
            // 
            // ToggleLockButton
            // 
            this.ToggleLockButton.ImageIndex = 0;
            this.ToggleLockButton.ImageList = this.LockImages;
            this.ToggleLockButton.Location = new System.Drawing.Point(12, 7);
            this.ToggleLockButton.Name = "ToggleLockButton";
            this.ToggleLockButton.Size = new System.Drawing.Size(26, 26);
            this.ToggleLockButton.TabIndex = 2;
            this.ToggleLockButton.UseVisualStyleBackColor = true;
            this.ToggleLockButton.Click += new System.EventHandler(this.ToggleLockButton_Click);
            // 
            // UrlTextBox
            // 
            this.UrlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UrlTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UrlTextBox.Location = new System.Drawing.Point(140, 6);
            this.UrlTextBox.Name = "UrlTextBox";
            this.UrlTextBox.Size = new System.Drawing.Size(188, 26);
            this.UrlTextBox.TabIndex = 5;
            // 
            // ToggleFullscreenButton
            // 
            this.ToggleFullscreenButton.ImageIndex = 0;
            this.ToggleFullscreenButton.ImageList = this.ResizeImages;
            this.ToggleFullscreenButton.Location = new System.Drawing.Point(44, 7);
            this.ToggleFullscreenButton.Name = "ToggleFullscreenButton";
            this.ToggleFullscreenButton.Size = new System.Drawing.Size(26, 26);
            this.ToggleFullscreenButton.TabIndex = 3;
            this.ToggleFullscreenButton.UseVisualStyleBackColor = true;
            this.ToggleFullscreenButton.Click += new System.EventHandler(this.ToggleFullscreenButton_Click);
            // 
            // RefreshButton
            // 
            this.RefreshButton.Image = global::Amphi.Properties.Resources.icon_refresh;
            this.RefreshButton.Location = new System.Drawing.Point(76, 7);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(26, 26);
            this.RefreshButton.TabIndex = 4;
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // EditButton
            // 
            this.EditButton.Image = global::Amphi.Properties.Resources.icon_edit;
            this.EditButton.Location = new System.Drawing.Point(108, 7);
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(26, 26);
            this.EditButton.TabIndex = 7;
            this.EditButton.UseVisualStyleBackColor = true;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // BrowserStatusBar
            // 
            this.BrowserStatusBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BrowserStatusBar.Location = new System.Drawing.Point(12, 187);
            this.BrowserStatusBar.Name = "BrowserStatusBar";
            this.BrowserStatusBar.Size = new System.Drawing.Size(335, 26);
            this.BrowserStatusBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.BrowserStatusBar.TabIndex = 6;
            // 
            // ToggleWidgetSettingsButton
            // 
            this.ToggleWidgetSettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ToggleWidgetSettingsButton.ImageIndex = 0;
            this.ToggleWidgetSettingsButton.ImageList = this.SettingsImages;
            this.ToggleWidgetSettingsButton.Location = new System.Drawing.Point(321, 3);
            this.ToggleWidgetSettingsButton.Name = "ToggleWidgetSettingsButton";
            this.ToggleWidgetSettingsButton.Size = new System.Drawing.Size(26, 26);
            this.ToggleWidgetSettingsButton.TabIndex = 1;
            this.ToggleWidgetSettingsButton.UseVisualStyleBackColor = true;
            this.ToggleWidgetSettingsButton.Click += new System.EventHandler(this.ToggleWidgetSettings_Click);
            // 
            // SettingsImages
            // 
            this.SettingsImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("SettingsImages.ImageStream")));
            this.SettingsImages.TransparentColor = System.Drawing.Color.Transparent;
            this.SettingsImages.Images.SetKeyName(0, "icon_up.png");
            this.SettingsImages.Images.SetKeyName(1, "icon_hamburger.png");
            // 
            // WidgetBrowser
            // 
            this.WidgetBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WidgetBrowser.Location = new System.Drawing.Point(0, 0);
            this.WidgetBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.WidgetBrowser.Name = "WidgetBrowser";
            this.WidgetBrowser.ScriptErrorsSuppressed = true;
            this.WidgetBrowser.Size = new System.Drawing.Size(372, 253);
            this.WidgetBrowser.TabIndex = 5;
            this.WidgetBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.WidgetBrowser_DocumentComplete);
            this.WidgetBrowser.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.WidgetBrowser_Navigating);
            // 
            // HtmlWidgetView
            // 
            this.AcceptButton = this.GoButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 295);
            this.Controls.Add(this.HtmlWidgetSplit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(253, 197);
            this.Name = "HtmlWidgetView";
            this.Text = "WidgetViewMin";
            this.Load += new System.EventHandler(this.HtmlWidgetView_Load);
            this.ResizeEnd += new System.EventHandler(this.Size_Changed);
            this.LocationChanged += new System.EventHandler(this.Location_Changed);
            this.SizeChanged += new System.EventHandler(this.Size_Changed);
            this.HtmlWidgetSplit.Panel1.ResumeLayout(false);
            this.HtmlWidgetSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HtmlWidgetSplit)).EndInit();
            this.HtmlWidgetSplit.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.BrowserPanel.ResumeLayout(false);
            this.BrowserControlPanel.ResumeLayout(false);
            this.BrowserControlPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList LockImages;
        private System.Windows.Forms.ImageList ResizeImages;
        private System.Windows.Forms.SplitContainer HtmlWidgetSplit;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Panel BrowserPanel;
        private System.Windows.Forms.ProgressBar BrowserStatusBar;
        private System.Windows.Forms.Button ToggleWidgetSettingsButton;
        private System.Windows.Forms.WebBrowser WidgetBrowser;
        private System.Windows.Forms.Button GoButton;
        private System.Windows.Forms.TextBox UrlTextBox;
        private System.Windows.Forms.Button ToggleLockButton;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Button ToggleFullscreenButton;
        private System.Windows.Forms.ImageList SettingsImages;
        private System.Windows.Forms.Button EditButton;
        private System.Windows.Forms.Panel BrowserControlPanel;
    }
}