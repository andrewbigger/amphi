﻿namespace Amphi.Views
{
    partial class AboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutDialog));
            this.CopyrightLabel = new MetroFramework.Controls.MetroLabel();
            this.VersionLabel = new MetroFramework.Controls.MetroLabel();
            this.ProductNameLabel = new MetroFramework.Controls.MetroLabel();
            this.ShareButton = new System.Windows.Forms.Button();
            this.WebButton = new System.Windows.Forms.Button();
            this.HelpLinkButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CopyrightLabel
            // 
            this.CopyrightLabel.AutoSize = true;
            this.CopyrightLabel.Location = new System.Drawing.Point(23, 113);
            this.CopyrightLabel.Margin = new System.Windows.Forms.Padding(3);
            this.CopyrightLabel.Name = "CopyrightLabel";
            this.CopyrightLabel.Size = new System.Drawing.Size(89, 19);
            this.CopyrightLabel.TabIndex = 15;
            this.CopyrightLabel.Text = "Normal Label";
            // 
            // VersionLabel
            // 
            this.VersionLabel.AutoSize = true;
            this.VersionLabel.Location = new System.Drawing.Point(23, 88);
            this.VersionLabel.Margin = new System.Windows.Forms.Padding(3);
            this.VersionLabel.Name = "VersionLabel";
            this.VersionLabel.Size = new System.Drawing.Size(79, 19);
            this.VersionLabel.TabIndex = 14;
            this.VersionLabel.Text = "Styled Label";
            this.VersionLabel.UseStyleColors = true;
            // 
            // ProductNameLabel
            // 
            this.ProductNameLabel.AutoSize = true;
            this.ProductNameLabel.Location = new System.Drawing.Point(23, 63);
            this.ProductNameLabel.Margin = new System.Windows.Forms.Padding(3);
            this.ProductNameLabel.Name = "ProductNameLabel";
            this.ProductNameLabel.Size = new System.Drawing.Size(91, 19);
            this.ProductNameLabel.TabIndex = 13;
            this.ProductNameLabel.Text = "ProductName";
            // 
            // ShareButton
            // 
            this.ShareButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ShareButton.Image = global::Amphi.Properties.Resources.icon_share;
            this.ShareButton.Location = new System.Drawing.Point(244, 122);
            this.ShareButton.Name = "ShareButton";
            this.ShareButton.Size = new System.Drawing.Size(28, 28);
            this.ShareButton.TabIndex = 18;
            this.ShareButton.UseVisualStyleBackColor = true;
            this.ShareButton.Click += new System.EventHandler(this.ShareButton_Click);
            // 
            // WebButton
            // 
            this.WebButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.WebButton.Image = global::Amphi.Properties.Resources.icon_web;
            this.WebButton.Location = new System.Drawing.Point(244, 88);
            this.WebButton.Name = "WebButton";
            this.WebButton.Size = new System.Drawing.Size(28, 28);
            this.WebButton.TabIndex = 17;
            this.WebButton.UseVisualStyleBackColor = true;
            this.WebButton.Click += new System.EventHandler(this.WebButton_Click);
            // 
            // HelpLinkButton
            // 
            this.HelpLinkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpLinkButton.Image = global::Amphi.Properties.Resources.icon_help;
            this.HelpLinkButton.Location = new System.Drawing.Point(244, 54);
            this.HelpLinkButton.Name = "HelpLinkButton";
            this.HelpLinkButton.Size = new System.Drawing.Size(28, 28);
            this.HelpLinkButton.TabIndex = 16;
            this.HelpLinkButton.UseVisualStyleBackColor = true;
            this.HelpLinkButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // AboutDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 173);
            this.Controls.Add(this.ShareButton);
            this.Controls.Add(this.WebButton);
            this.Controls.Add(this.HelpLinkButton);
            this.Controls.Add(this.CopyrightLabel);
            this.Controls.Add(this.VersionLabel);
            this.Controls.Add(this.ProductNameLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(601, 396);
            this.MinimizeBox = false;
            this.Name = "AboutDialog";
            this.Resizable = false;
            this.ShowInTaskbar = false;
            this.Text = "About";
            this.Load += new System.EventHandler(this.SettingsView_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel CopyrightLabel;
        private MetroFramework.Controls.MetroLabel VersionLabel;
        private MetroFramework.Controls.MetroLabel ProductNameLabel;
        private System.Windows.Forms.Button HelpLinkButton;
        private System.Windows.Forms.Button WebButton;
        private System.Windows.Forms.Button ShareButton;

    }
}