﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amphi.Controllers;
using Amphi.Helpers;
using Amphi.Models;
using Amphi.Views.Settings;
using MetroFramework.Forms;

namespace Amphi.Views
{
    public partial class SettingsView : MetroForm
    {
        #region Constructor
        public SettingsView()
        {
            this.InitializeComponent();
            this.SetTheme();
        }
        #endregion

        #region Public Form Methods
        public void SetTheme()
        {
            UIHelper.SetTheme(this);
            UIHelper.SetTheme(this.SettingsTabs);
            UIHelper.SetTheme(this.UiTab);
            UIHelper.SetTheme(this.UiTitleLabel);
            UIHelper.SetTheme(this.UiTitleTextBox);
            UIHelper.SetTheme(this.UiThemeLabel);
            UIHelper.SetTheme(this.UiHighlightLabel);
            UIHelper.SetTheme(this.ApplyButton);
            UIHelper.SetTheme(this.SaveButton);
            UIHelper.SetTheme(this.UiHighlightComboBox);
            UIHelper.SetTheme(this.UiThemeComboBox);
            UIHelper.SetTheme(this.ModesTab);
            UIHelper.SetTheme(this.ModesLabel);
            UIHelper.SetTheme(this.ModeListComboBox);
            UIHelper.SetTheme(this.AddModeButton);
            UIHelper.SetTheme(this.RemoveModeButton);
            UIHelper.SetTheme(this.EditModeButton);
            UIHelper.SetTheme(this.WidgetsLabel);
            UIHelper.SetTheme(this.WidgetListComboBox);
            UIHelper.SetTheme(this.AddWidgetButton);
            UIHelper.SetTheme(this.RemoveWidgetButton);
            UIHelper.SetTheme(this.EditWidgetButton);
            UIHelper.SetTheme(this.ModesAndWidgetsSettingsInstructionsLabel);
            UIHelper.SetTheme(this.UISettingsInstructionsLabel);
            UIHelper.SetTheme(this.ConfigurationSettingsInstructionsLabel);
            UIHelper.SetTheme(this.ExportModesButton);
            UIHelper.SetTheme(this.ImportModesButton);
            UIHelper.SetTheme(this.ConfigTab);
        }
        #endregion

        #region Private Methods
        private void SetDefaults()
        {
            UIHelper.SetSelectedItem(this.UiThemeComboBox, Properties.Settings.Default.Theme);
            UIHelper.SetSelectedItem(this.UiHighlightComboBox, Properties.Settings.Default.Color);
            UIHelper.SetSelectedItem(this.ModeListComboBox, Properties.Settings.Default.ActiveMode);
        }

        private void ToggleWidgetControls(bool show)
        {
            this.WidgetListComboBox.Visible = show;
            this.AddWidgetButton.Visible = show;
            this.RemoveWidgetButton.Visible = show;
            this.EditWidgetButton.Visible = show;
            this.WidgetsLabel.Visible = show;  
        }

        private void PopulateModes()
        {
            this.ModeListComboBox.Items.Clear();
            foreach (Mode m in ApplicationController.Modes.Index)
            {
                this.ModeListComboBox.Items.Add(m.Name);
            }
        }

        private void PopulateWidgets()
        {
            this.WidgetListComboBox.Items.Clear();
            Mode activeMode = ApplicationController.Modes.Find(
                    this.ModeListComboBox.SelectedItem.ToString())[0];

            if (activeMode != null)
            {
                UIHelper.PopulateWidgetList(this.WidgetListComboBox, activeMode);
                this.ToggleWidgetControls(true);
            }
            else
            {
                this.ToggleWidgetControls(false);
            }
        }

        private string SelectedMode()
        {
            return this.ModeListComboBox.SelectedItem.ToString();
        }

        private string SelectedWidget()
        {
            return this.WidgetListComboBox.SelectedItem.ToString();
        }

        private bool HasSelectedMode()
        {
            return ModeListComboBox.SelectedItem != null;
        }

        private bool HasSelectedWidget()
        {
            return WidgetListComboBox.SelectedItem != null;
        }
        #endregion

        #region Private Event Handlers
        private void SettingsView_Load(object sender, EventArgs e)
        {
            this.SetDefaults();
            this.PopulateModes();
        }

        private void ModeListComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.HasSelectedMode())
            {
                this.PopulateWidgets();
            }
        }

        private void AddModeButton_Click(object sender, EventArgs e)
        {
            ApplicationHelper.AddMode(this);
            this.PopulateModes();
            this.ToggleWidgetControls(false);
        }

        private void RemoveModeButton_Click(object sender, EventArgs e)
        {
            if (this.HasSelectedMode())
            {
                ApplicationHelper.RemoveMode(this, this.SelectedMode());
                this.PopulateModes();
                this.ToggleWidgetControls(false);
            }
        }

        private void EditModeButton_Click(object sender, EventArgs e)
        {
            if (this.HasSelectedMode())
            {
                ApplicationHelper.EditMode(this, this.SelectedMode());
                this.PopulateModes();
                this.ToggleWidgetControls(false);
            }
        }

        private void AddWidgetButton_Click(object sender, EventArgs e)
        {
            ApplicationHelper.AddWidget(this, this.SelectedMode());
            this.PopulateWidgets();
        }

        private void EditWidgetButton_Click(object sender, EventArgs e)
        {
            if (this.HasSelectedWidget())
            {
                ApplicationHelper.EditWidget(this, this.SelectedMode(), this.SelectedWidget());
                this.PopulateWidgets();
            }
        }

        private void RemoveWidgetButton_Click(object sender, EventArgs e)
        {
            if (this.HasSelectedWidget())
            {
                ApplicationHelper.DeleteWidget(this, this.SelectedMode(), this.SelectedWidget());
                this.PopulateWidgets();
            }
        }

        private void ExportModesButton_Click(object sender, EventArgs e)
        {
            ApplicationHelper.ExportConfig(this);
            this.PopulateModes();
        }

        private void ImportModesButton_Click(object sender, EventArgs e)
        {
            ApplicationHelper.ImportConfig(this);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            this.SetTheme(); // TODO: Move action to controller
            this.Refresh();
        }
        #endregion
    }
}