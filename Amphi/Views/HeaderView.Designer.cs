﻿namespace Amphi
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.ModeComboBox = new MetroFramework.Controls.MetroComboBox();
            this.AddWidgetToActiveModeButton = new System.Windows.Forms.Button();
            this.AboutButton = new System.Windows.Forms.Button();
            this.SettingsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ModeComboBox
            // 
            this.ModeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ModeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Amphi.Properties.Settings.Default, "Color", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ModeComboBox.FormattingEnabled = true;
            this.ModeComboBox.ItemHeight = 23;
            this.ModeComboBox.Location = new System.Drawing.Point(322, 25);
            this.ModeComboBox.Name = "ModeComboBox";
            this.ModeComboBox.Size = new System.Drawing.Size(305, 29);
            this.ModeComboBox.TabIndex = 40;
            this.ModeComboBox.Text = global::Amphi.Properties.Settings.Default.Color;
            this.ModeComboBox.UseSelectable = true;
            this.ModeComboBox.SelectedIndexChanged += new System.EventHandler(this.ModeComboBox_SelectedIndexChanged);
            // 
            // AddWidgetToActiveModeButton
            // 
            this.AddWidgetToActiveModeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AddWidgetToActiveModeButton.Image = global::Amphi.Properties.Resources.icon_plus;
            this.AddWidgetToActiveModeButton.Location = new System.Drawing.Point(288, 25);
            this.AddWidgetToActiveModeButton.Name = "AddWidgetToActiveModeButton";
            this.AddWidgetToActiveModeButton.Size = new System.Drawing.Size(28, 29);
            this.AddWidgetToActiveModeButton.TabIndex = 41;
            this.AddWidgetToActiveModeButton.UseVisualStyleBackColor = true;
            this.AddWidgetToActiveModeButton.Click += new System.EventHandler(this.AddWidgetToActiveModeButton_Click);
            // 
            // AboutButton
            // 
            this.AboutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AboutButton.Image = global::Amphi.Properties.Resources.icon_about;
            this.AboutButton.Location = new System.Drawing.Point(633, 25);
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Size = new System.Drawing.Size(28, 29);
            this.AboutButton.TabIndex = 9;
            this.AboutButton.UseVisualStyleBackColor = true;
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // SettingsButton
            // 
            this.SettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsButton.Image = global::Amphi.Properties.Resources.icon_cog;
            this.SettingsButton.Location = new System.Drawing.Point(667, 25);
            this.SettingsButton.Name = "SettingsButton";
            this.SettingsButton.Size = new System.Drawing.Size(28, 29);
            this.SettingsButton.TabIndex = 7;
            this.SettingsButton.UseVisualStyleBackColor = true;
            this.SettingsButton.Click += new System.EventHandler(this.SettingsButton_Click);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(701, 62);
            this.Controls.Add(this.AddWidgetToActiveModeButton);
            this.Controls.Add(this.ModeComboBox);
            this.Controls.Add(this.AboutButton);
            this.Controls.Add(this.SettingsButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainView";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Default;
            this.TransparencyKey = System.Drawing.Color.Empty;
            this.Load += new System.EventHandler(this.MainView_Load);
            this.LocationChanged += new System.EventHandler(this.HeaderView_Move);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SettingsButton;
        private System.Windows.Forms.Button AboutButton;
        private MetroFramework.Controls.MetroComboBox ModeComboBox;
        private System.Windows.Forms.Button AddWidgetToActiveModeButton;




    }
}