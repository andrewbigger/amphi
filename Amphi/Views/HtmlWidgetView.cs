﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amphi.Controllers;
using Amphi.Helpers;
using Amphi.Models;
using Amphi.Views.Settings;

namespace Amphi.Views
{
    public partial class HtmlWidgetView : Form
    {
        private HtmlWidget currentWidget;
        private bool loading;

        #region Constructor
        public HtmlWidgetView(HtmlWidget viewWidget)
        {
            this.InitializeComponent();
            this.ActiveWidget = viewWidget;
            this.Text = viewWidget.Name;
            this.ToggleEditing();
            this.loading = true;
        }
        #endregion

        #region Public Properties
        public bool Locked
        {
            get
            {
                return this.currentWidget.Locked;
            }

            set
            {
                this.currentWidget.Locked = value;
            }
        }

        public bool Maximised
        {
            get
            {
                return this.WindowState == FormWindowState.Maximized;
            }
        }
        
        public HtmlWidget ActiveWidget
        {
            get
            {
                return this.currentWidget;
            }

            set
            {
                this.currentWidget = value;
            }
        }
        #endregion

        #region Private Properties
        private bool Editing
        {
            get
            {
                return this.HtmlWidgetSplit.Panel1Collapsed;
            }
        }
        #endregion

        #region Public Form Methods
        public void RefreshBrowser()
        {
           this.WidgetBrowser.Refresh();
        }

        public void ToggleLock()
        {
            if (this.Locked)
            {
                this.Locked = false;
            }
            else
            {
                this.Locked = true;
            }

            this.ApplyLock();
            this.ToggleEditing();
            ApplicationController.Save();
        }

        private void ApplyLock()
        {
            UIHelper.ToggleLock(this, this.Locked);
            this.ToggleLockButton.ImageIndex = Convert.ToInt32(this.Locked);
        }

        public void ToggleFullScreen()
        {
            UIHelper.ToggleFullScreen(this, this.Maximised);
            this.ToggleFullscreenButton.ImageIndex = Convert.ToInt32(this.Maximised);

            this.ApplyLock();

            // Disable lock button in maximised context
            this.ToggleLockButton.Enabled = this.Maximised == false;

            this.ToggleEditing();
        }

        public void SizeWidget(HtmlWidgetView form, string vertAlign, string horizAlign, int horizFit = 3, int vertFit = 2)
        {
            Screen targetScreen = UIHelper.WindowScreen(form);
            Size widgetSize = UIHelper.WindowBounds(targetScreen, horizFit, vertFit);
            Point position = UIHelper.WindowLocation(targetScreen, widgetSize, vertAlign, horizAlign);
            this.ActiveWidget.Size = widgetSize;
            this.ActiveWidget.Location = position;
            UIHelper.PositionWindow(this, ActiveWidget.Location);
            UIHelper.SizeWindow(this, ActiveWidget.Size);
            ApplicationController.Save();
        }

        public void ApplyWidget(bool navigate = true)
        {
            this.Location = ActiveWidget.Location;
            this.Size = ActiveWidget.Size;
            this.ApplyLock();

            if (navigate)
            {
                this.Navigate(this.currentWidget.GetUrl.ToString());
            }

            this.loading = false;
        }
        #endregion

        #region Public Event Handlers
        public void Snap_TopLeft(object sender, EventArgs e)
        {
            this.SizeWidget(this, "top", "left");
        }

        public void Snap_TopCentre(object sender, EventArgs e)
        {
            this.SizeWidget(this, "top", "centre");
        }

        public void Snap_TopRight(object sender, EventArgs e)
        {
            this.SizeWidget(this, "top", "right");
        }

        public void Snap_CentreLeft(object sender, EventArgs e)
        {
            this.SizeWidget(this, "top", "left", 2, 1);
        }

        public void Snap_CentreRight(object sender, EventArgs e)
        {
            this.SizeWidget(this, "top", "right", 2, 1);
        }

        public void Snap_Centre(object sender, EventArgs e)
        {
            this.SizeWidget(this, "top", "left", 1, 1);
        }

        public void Snap_LeftThird(object sender, EventArgs e)
        {
            this.SizeWidget(this, "top", "left", 3, 1);
        }

        public void Snap_CentreThird(object sender, EventArgs e)
        {
            this.SizeWidget(this, "top", "centre", 3, 1);
        }

        public void Snap_RightThird(object sender, EventArgs e)
        {
            this.SizeWidget(this, "top", "right", 3, 1);
        }

        public void Snap_BottomLeft(object sender, EventArgs e)
        {
            this.SizeWidget(this, "bottom", "left");
        }

        public void Snap_BottomCentre(object sender, EventArgs e)
        {
            this.SizeWidget(this, "bottom", "centre");
        }

        public void Snap_BottomRight(object sender, EventArgs e)
        {
            this.SizeWidget(this, "bottom", "right");
        }
        #endregion

        #region Private Form Methods
        private void SaveWidget()
        {
            ApplicationController.Save();
        }

        private void UpdateAddressTextBox()
        {
           if (this.WidgetBrowser.Url != null)
           {
               this.UrlTextBox.Text = this.WidgetBrowser.Url.ToString();
           }
           else
           {
               this.UrlTextBox.Text = "about:blank";
           }
        }

        private void Navigate(string url)
        {
            this.WidgetBrowser.Navigate(url);
        }

        private void ToggleEditing()
        {
            if (this.Editing == true)
            {
                this.HtmlWidgetSplit.Panel1Collapsed = false;
            }
            else
            {
                this.HtmlWidgetSplit.Panel1Collapsed = true;
            }

            this.ToggleWidgetSettingsButton.ImageIndex = Convert.ToInt32(this.Editing);
        }
        #endregion
        
        #region Private Event Handlers
        private void HtmlWidgetView_Load(object sender, EventArgs e)
        {
            this.ApplyWidget();
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            this.RefreshBrowser();
            this.UpdateAddressTextBox();
        }

        private void ToggleFullscreenButton_Click(object sender, EventArgs e)
        {
            this.ToggleFullScreen();
        }

        private void ToggleLockButton_Click(object sender, EventArgs e)
        {
            this.ToggleLock();
        }

        private void ToggleWidgetSettings_Click(object sender, EventArgs e)
        {
            this.ToggleEditing();
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            try
            {
                HtmlWidgetSettingsView settings = new HtmlWidgetSettingsView(this.ActiveWidget);
                settings.ShowDialog();
                this.ApplyWidget();
                this.ToggleEditing();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(
                    this, 
                    "Oops! Amphi has lost your widget. Please contact support. \n \n" + ex.Message, 
                    "Critical Error", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Error); 
            }
        }

        private void GoButton_Click(object sender, EventArgs e)
        {
            this.Navigate(this.UrlTextBox.Text);
        }

        private void WidgetBrowser_DocumentComplete(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            this.BrowserStatusBar.Visible = false;
            this.UpdateAddressTextBox();
            this.Text = WidgetBrowser.DocumentTitle;
        }

        private void WidgetBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            this.Text = this.currentWidget.Name;
            this.UpdateAddressTextBox();
            this.BrowserStatusBar.Visible = true;
        }

        private void Location_Changed(object sender, EventArgs e)
        {
            if (loading == false)
            {
                this.ActiveWidget.Location = this.Location;
                ApplicationController.Save();
            }
        }

        private void Size_Changed(object sender, EventArgs e)
        {
            if (loading == false)
            {
                this.ActiveWidget.Size = this.Size;
                ApplicationController.Save();
            }
        }
        #endregion
    }
}
