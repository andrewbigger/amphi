﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amphi.Controllers;
using Amphi.Helpers;
using Amphi.Models;
using MetroFramework.Forms;

namespace Amphi.Views.Settings
{
    public partial class HtmlWidgetSettingsView : MetroForm
    {
        private HtmlWidgetView targetWidgetView;
        private HtmlWidget activeWidget;
        private bool create;

        #region Constructor
        public HtmlWidgetSettingsView(HtmlWidget targetWidget, bool createWidget = false)
        {
            this.InitializeComponent();
            try
            {
                this.targetWidgetView = ApplicationController.WidgetViews.Find(targetWidget)[0];
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw e;
            }

            this.activeWidget = targetWidget;
            this.create = createWidget;
            this.SetTheme();
            this.SetupForm();
        }
        #endregion

        #region Public Properties
        public HtmlWidget ActiveWidget
        {
            get
            {
                return this.activeWidget;
            }

            set
            {
                this.activeWidget = value;
            }
        }
        #endregion

        #region Public Form Methods
        public void SetTheme()
        {
            UIHelper.SetTheme(this);
            UIHelper.SetTheme(this.WidgetNameLabel);
            UIHelper.SetTheme(this.WidgetNameTextBox);
            UIHelper.SetTheme(this.WidgetUrlLabel);
            UIHelper.SetTheme(this.WidgetUrlTextBox);
            UIHelper.SetTheme(this.TopLeftButton);
            UIHelper.SetTheme(this.TopCentreButton);
            UIHelper.SetTheme(this.TopRightButton);
            UIHelper.SetTheme(this.CentreThirdButton);
            UIHelper.SetTheme(this.CentreLeftButton);
            UIHelper.SetTheme(this.CentreRightButton);
            UIHelper.SetTheme(this.CentreButton);
            UIHelper.SetTheme(this.LeftHalfButton);
            UIHelper.SetTheme(this.RightHalfButton);
            UIHelper.SetTheme(this.BottomLeftButton);
            UIHelper.SetTheme(this.BottomCentreButton);
            UIHelper.SetTheme(this.BottomRightButton);
            UIHelper.SetTheme(this.SaveButton);
            UIHelper.SetTheme(this.ScreenPositionLabel);
            UIHelper.SetTheme(this.CancelHtmlWidgetEditButton);
            UIHelper.SetTheme(this.SaveButton);
        }
        #endregion

        #region Private Form Methods
        private void SetupForm()
        {
            if (this.create)
            {
                this.Text = "Create new Widget";
                this.WidgetNameTextBox.Text = string.Empty;
                this.WidgetUrlTextBox.Text = string.Empty;
            }
            else
            {
                this.Text = this.ActiveWidget.Name + " Settings";
                this.WidgetNameTextBox.Text = this.ActiveWidget.Name;
                this.WidgetUrlTextBox.Text = this.ActiveWidget.GetUrl.ToString();
            }
        }

        private bool ApplySettings()
        {
            string applyAction;
            if (this.create)
            {
                applyAction = "update";
            }
            else
            {
                applyAction = "create";
            }

            try
            {
                this.ActiveWidget.Name = this.WidgetNameTextBox.Text;
                this.ActiveWidget.SetUrl = this.WidgetUrlTextBox.Text;
                this.ActiveWidget.Location = this.targetWidgetView.Location;
                return true;
            }
            catch (InvalidWidgetNameException)
            {
                MessageBox.Show(
                    this,
                    "Unable to " + applyAction + " Widget because of an invalid name, please try again",
                    "Invalid Widget Name",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
            catch (InvalidWidgetUrlException)
            {
                MessageBox.Show(
                    this,
                    "Unable to " + applyAction + " Widget because of an invalid URL, please try again",
                    "Invalid Widget URL",
                    MessageBoxButtons.OK);
                return false;
            }
        }
        #endregion

        #region Private Event Handlers
        private void TopLeftButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_TopLeft(sender, e);
        }

        private void TopCentreButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_TopCentre(sender, e);
        }

        private void TopRightButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_TopRight(sender, e);
        }

        private void LeftHalfButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_CentreLeft(sender, e);
        }

        private void RightHalfButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_CentreRight(sender, e);
        }

        private void CentreButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_Centre(sender, e);
        }

        private void CentreLeftButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_LeftThird(sender, e);
        }

        private void CentreThirdButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_CentreThird(sender, e);
        }

        private void CentreRightButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_RightThird(sender, e);
        }

        private void BottomLeftButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_BottomLeft(sender, e);
        }

        private void BottomCentreButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_BottomCentre(sender, e);
        }

        private void BottomRightButton_Click(object sender, EventArgs e)
        {
            this.targetWidgetView.Snap_BottomRight(sender, e);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            bool canClose = this.ApplySettings();
            if (canClose)
            {
                this.Close();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
