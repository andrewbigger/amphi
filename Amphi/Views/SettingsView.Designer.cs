﻿namespace Amphi.Views
{
    partial class SettingsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsView));
            this.SaveButton = new MetroFramework.Controls.MetroButton();
            this.SettingsTabs = new MetroFramework.Controls.MetroTabControl();
            this.ModesTab = new MetroFramework.Controls.MetroTabPage();
            this.EditWidgetButton = new MetroFramework.Controls.MetroButton();
            this.RemoveWidgetButton = new MetroFramework.Controls.MetroButton();
            this.AddWidgetButton = new MetroFramework.Controls.MetroButton();
            this.WidgetListComboBox = new MetroFramework.Controls.MetroComboBox();
            this.WidgetsLabel = new MetroFramework.Controls.MetroLabel();
            this.EditModeButton = new MetroFramework.Controls.MetroButton();
            this.RemoveModeButton = new MetroFramework.Controls.MetroButton();
            this.AddModeButton = new MetroFramework.Controls.MetroButton();
            this.ModeListComboBox = new MetroFramework.Controls.MetroComboBox();
            this.ModesLabel = new MetroFramework.Controls.MetroLabel();
            this.UiTab = new MetroFramework.Controls.MetroTabPage();
            this.UiHighlightComboBox = new MetroFramework.Controls.MetroComboBox();
            this.UiThemeComboBox = new MetroFramework.Controls.MetroComboBox();
            this.UiThemeLabel = new MetroFramework.Controls.MetroLabel();
            this.UiHighlightLabel = new MetroFramework.Controls.MetroLabel();
            this.UiTitleLabel = new MetroFramework.Controls.MetroLabel();
            this.UiTitleTextBox = new MetroFramework.Controls.MetroTextBox();
            this.ApplyButton = new MetroFramework.Controls.MetroButton();
            this.ConfigTab = new MetroFramework.Controls.MetroTabPage();
            this.ExportModesButton = new MetroFramework.Controls.MetroButton();
            this.ImportModesButton = new MetroFramework.Controls.MetroButton();
            this.ConfigurationSettingsInstructionsLabel = new MetroFramework.Controls.MetroLabel();
            this.ModesAndWidgetsSettingsInstructionsLabel = new MetroFramework.Controls.MetroLabel();
            this.UISettingsInstructionsLabel = new MetroFramework.Controls.MetroLabel();
            this.SettingsTabs.SuspendLayout();
            this.ModesTab.SuspendLayout();
            this.UiTab.SuspendLayout();
            this.ConfigTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveButton.Highlight = true;
            this.SaveButton.Location = new System.Drawing.Point(451, 340);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(127, 37);
            this.SaveButton.TabIndex = 7;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseSelectable = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // SettingsTabs
            // 
            this.SettingsTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsTabs.Controls.Add(this.ModesTab);
            this.SettingsTabs.Controls.Add(this.UiTab);
            this.SettingsTabs.Controls.Add(this.ConfigTab);
            this.SettingsTabs.Location = new System.Drawing.Point(23, 63);
            this.SettingsTabs.Name = "SettingsTabs";
            this.SettingsTabs.SelectedIndex = 2;
            this.SettingsTabs.Size = new System.Drawing.Size(555, 271);
            this.SettingsTabs.TabIndex = 10;
            this.SettingsTabs.UseSelectable = true;
            // 
            // ModesTab
            // 
            this.ModesTab.Controls.Add(this.ModesAndWidgetsSettingsInstructionsLabel);
            this.ModesTab.Controls.Add(this.EditWidgetButton);
            this.ModesTab.Controls.Add(this.RemoveWidgetButton);
            this.ModesTab.Controls.Add(this.AddWidgetButton);
            this.ModesTab.Controls.Add(this.WidgetListComboBox);
            this.ModesTab.Controls.Add(this.WidgetsLabel);
            this.ModesTab.Controls.Add(this.EditModeButton);
            this.ModesTab.Controls.Add(this.RemoveModeButton);
            this.ModesTab.Controls.Add(this.AddModeButton);
            this.ModesTab.Controls.Add(this.ModeListComboBox);
            this.ModesTab.Controls.Add(this.ModesLabel);
            this.ModesTab.HorizontalScrollbarBarColor = true;
            this.ModesTab.HorizontalScrollbarHighlightOnWheel = false;
            this.ModesTab.HorizontalScrollbarSize = 10;
            this.ModesTab.Location = new System.Drawing.Point(4, 38);
            this.ModesTab.Name = "ModesTab";
            this.ModesTab.Padding = new System.Windows.Forms.Padding(25);
            this.ModesTab.Size = new System.Drawing.Size(547, 229);
            this.ModesTab.TabIndex = 2;
            this.ModesTab.Text = "Modes and Widgets";
            this.ModesTab.VerticalScrollbarBarColor = true;
            this.ModesTab.VerticalScrollbarHighlightOnWheel = false;
            this.ModesTab.VerticalScrollbarSize = 10;
            this.ModesTab.Visible = false;
            // 
            // EditWidgetButton
            // 
            this.EditWidgetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EditWidgetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EditWidgetButton.Location = new System.Drawing.Point(389, 195);
            this.EditWidgetButton.Name = "EditWidgetButton";
            this.EditWidgetButton.Size = new System.Drawing.Size(155, 31);
            this.EditWidgetButton.TabIndex = 37;
            this.EditWidgetButton.Text = "Edit Selected";
            this.EditWidgetButton.UseSelectable = true;
            this.EditWidgetButton.Visible = false;
            this.EditWidgetButton.Click += new System.EventHandler(this.EditWidgetButton_Click);
            // 
            // RemoveWidgetButton
            // 
            this.RemoveWidgetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RemoveWidgetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RemoveWidgetButton.Location = new System.Drawing.Point(228, 195);
            this.RemoveWidgetButton.Name = "RemoveWidgetButton";
            this.RemoveWidgetButton.Size = new System.Drawing.Size(155, 31);
            this.RemoveWidgetButton.TabIndex = 36;
            this.RemoveWidgetButton.Text = "Remove Selected";
            this.RemoveWidgetButton.UseSelectable = true;
            this.RemoveWidgetButton.Visible = false;
            this.RemoveWidgetButton.Click += new System.EventHandler(this.RemoveWidgetButton_Click);
            // 
            // AddWidgetButton
            // 
            this.AddWidgetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddWidgetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddWidgetButton.Location = new System.Drawing.Point(70, 195);
            this.AddWidgetButton.Name = "AddWidgetButton";
            this.AddWidgetButton.Size = new System.Drawing.Size(152, 31);
            this.AddWidgetButton.TabIndex = 35;
            this.AddWidgetButton.Text = "Add New";
            this.AddWidgetButton.UseSelectable = true;
            this.AddWidgetButton.Visible = false;
            this.AddWidgetButton.Click += new System.EventHandler(this.AddWidgetButton_Click);
            // 
            // WidgetListComboBox
            // 
            this.WidgetListComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Amphi.Properties.Settings.Default, "Color", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.WidgetListComboBox.FormattingEnabled = true;
            this.WidgetListComboBox.ItemHeight = 23;
            this.WidgetListComboBox.Items.AddRange(new object[] {
            "Blue",
            "Orange",
            "Red",
            "Green",
            "Black",
            "Brown",
            "Lime",
            "Magenta",
            "Pink",
            "Purple",
            "Silver",
            "Teal",
            "White",
            "Yellow"});
            this.WidgetListComboBox.Location = new System.Drawing.Point(70, 160);
            this.WidgetListComboBox.Name = "WidgetListComboBox";
            this.WidgetListComboBox.Size = new System.Drawing.Size(474, 29);
            this.WidgetListComboBox.TabIndex = 34;
            this.WidgetListComboBox.Text = global::Amphi.Properties.Settings.Default.Color;
            this.WidgetListComboBox.UseSelectable = true;
            this.WidgetListComboBox.Visible = false;
            // 
            // WidgetsLabel
            // 
            this.WidgetsLabel.AutoSize = true;
            this.WidgetsLabel.Location = new System.Drawing.Point(7, 160);
            this.WidgetsLabel.Name = "WidgetsLabel";
            this.WidgetsLabel.Size = new System.Drawing.Size(57, 19);
            this.WidgetsLabel.TabIndex = 33;
            this.WidgetsLabel.Text = "Widgets";
            this.WidgetsLabel.Visible = false;
            // 
            // EditModeButton
            // 
            this.EditModeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EditModeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EditModeButton.Location = new System.Drawing.Point(389, 90);
            this.EditModeButton.Name = "EditModeButton";
            this.EditModeButton.Size = new System.Drawing.Size(155, 31);
            this.EditModeButton.TabIndex = 32;
            this.EditModeButton.Text = "Edit Selected";
            this.EditModeButton.UseSelectable = true;
            this.EditModeButton.Click += new System.EventHandler(this.EditModeButton_Click);
            // 
            // RemoveModeButton
            // 
            this.RemoveModeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RemoveModeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RemoveModeButton.Location = new System.Drawing.Point(228, 90);
            this.RemoveModeButton.Name = "RemoveModeButton";
            this.RemoveModeButton.Size = new System.Drawing.Size(155, 31);
            this.RemoveModeButton.TabIndex = 31;
            this.RemoveModeButton.Text = "Remove Selected";
            this.RemoveModeButton.UseSelectable = true;
            this.RemoveModeButton.Click += new System.EventHandler(this.RemoveModeButton_Click);
            // 
            // AddModeButton
            // 
            this.AddModeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddModeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddModeButton.Location = new System.Drawing.Point(70, 90);
            this.AddModeButton.Name = "AddModeButton";
            this.AddModeButton.Size = new System.Drawing.Size(152, 31);
            this.AddModeButton.TabIndex = 30;
            this.AddModeButton.Text = "Add New";
            this.AddModeButton.UseSelectable = true;
            this.AddModeButton.Click += new System.EventHandler(this.AddModeButton_Click);
            // 
            // ModeListComboBox
            // 
            this.ModeListComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Amphi.Properties.Settings.Default, "Color", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ModeListComboBox.FormattingEnabled = true;
            this.ModeListComboBox.ItemHeight = 23;
            this.ModeListComboBox.Location = new System.Drawing.Point(70, 55);
            this.ModeListComboBox.Name = "ModeListComboBox";
            this.ModeListComboBox.Size = new System.Drawing.Size(474, 29);
            this.ModeListComboBox.TabIndex = 29;
            this.ModeListComboBox.Text = global::Amphi.Properties.Settings.Default.Color;
            this.ModeListComboBox.UseSelectable = true;
            this.ModeListComboBox.SelectedIndexChanged += new System.EventHandler(this.ModeListComboBox_SelectedIndexChanged);
            // 
            // ModesLabel
            // 
            this.ModesLabel.AutoSize = true;
            this.ModesLabel.Location = new System.Drawing.Point(15, 55);
            this.ModesLabel.Name = "ModesLabel";
            this.ModesLabel.Size = new System.Drawing.Size(49, 19);
            this.ModesLabel.TabIndex = 28;
            this.ModesLabel.Text = "Modes";
            // 
            // UiTab
            // 
            this.UiTab.AutoScroll = true;
            this.UiTab.Controls.Add(this.UISettingsInstructionsLabel);
            this.UiTab.Controls.Add(this.UiHighlightComboBox);
            this.UiTab.Controls.Add(this.UiThemeComboBox);
            this.UiTab.Controls.Add(this.UiThemeLabel);
            this.UiTab.Controls.Add(this.UiHighlightLabel);
            this.UiTab.Controls.Add(this.UiTitleLabel);
            this.UiTab.Controls.Add(this.UiTitleTextBox);
            this.UiTab.HorizontalScrollbar = true;
            this.UiTab.HorizontalScrollbarBarColor = true;
            this.UiTab.HorizontalScrollbarHighlightOnWheel = false;
            this.UiTab.HorizontalScrollbarSize = 10;
            this.UiTab.Location = new System.Drawing.Point(4, 38);
            this.UiTab.Name = "UiTab";
            this.UiTab.Padding = new System.Windows.Forms.Padding(25);
            this.UiTab.Size = new System.Drawing.Size(547, 229);
            this.UiTab.TabIndex = 0;
            this.UiTab.Text = "UI";
            this.UiTab.VerticalScrollbar = true;
            this.UiTab.VerticalScrollbarBarColor = true;
            this.UiTab.VerticalScrollbarHighlightOnWheel = false;
            this.UiTab.VerticalScrollbarSize = 10;
            // 
            // UiHighlightComboBox
            // 
            this.UiHighlightComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Amphi.Properties.Settings.Default, "Color", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.UiHighlightComboBox.FormattingEnabled = true;
            this.UiHighlightComboBox.ItemHeight = 23;
            this.UiHighlightComboBox.Items.AddRange(new object[] {
            "Blue",
            "Orange",
            "Red",
            "Green",
            "Black",
            "Brown",
            "Lime",
            "Magenta",
            "Pink",
            "Purple",
            "Silver",
            "Teal",
            "White",
            "Yellow"});
            this.UiHighlightComboBox.Location = new System.Drawing.Point(72, 102);
            this.UiHighlightComboBox.Name = "UiHighlightComboBox";
            this.UiHighlightComboBox.Size = new System.Drawing.Size(449, 29);
            this.UiHighlightComboBox.TabIndex = 27;
            this.UiHighlightComboBox.Text = global::Amphi.Properties.Settings.Default.Color;
            this.UiHighlightComboBox.UseSelectable = true;
            // 
            // UiThemeComboBox
            // 
            this.UiThemeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Amphi.Properties.Settings.Default, "Theme", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.UiThemeComboBox.FormattingEnabled = true;
            this.UiThemeComboBox.ItemHeight = 23;
            this.UiThemeComboBox.Items.AddRange(new object[] {
            "Light",
            "Dark"});
            this.UiThemeComboBox.Location = new System.Drawing.Point(72, 137);
            this.UiThemeComboBox.Name = "UiThemeComboBox";
            this.UiThemeComboBox.Size = new System.Drawing.Size(449, 29);
            this.UiThemeComboBox.TabIndex = 26;
            this.UiThemeComboBox.Text = global::Amphi.Properties.Settings.Default.Theme;
            this.UiThemeComboBox.UseSelectable = true;
            // 
            // UiThemeLabel
            // 
            this.UiThemeLabel.AutoSize = true;
            this.UiThemeLabel.Location = new System.Drawing.Point(17, 137);
            this.UiThemeLabel.Name = "UiThemeLabel";
            this.UiThemeLabel.Size = new System.Drawing.Size(49, 19);
            this.UiThemeLabel.TabIndex = 24;
            this.UiThemeLabel.Text = "Theme";
            // 
            // UiHighlightLabel
            // 
            this.UiHighlightLabel.AutoSize = true;
            this.UiHighlightLabel.Location = new System.Drawing.Point(5, 102);
            this.UiHighlightLabel.Name = "UiHighlightLabel";
            this.UiHighlightLabel.Size = new System.Drawing.Size(61, 19);
            this.UiHighlightLabel.TabIndex = 23;
            this.UiHighlightLabel.Text = "Highlight";
            // 
            // UiTitleLabel
            // 
            this.UiTitleLabel.AutoSize = true;
            this.UiTitleLabel.Location = new System.Drawing.Point(33, 64);
            this.UiTitleLabel.Name = "UiTitleLabel";
            this.UiTitleLabel.Size = new System.Drawing.Size(33, 19);
            this.UiTitleLabel.TabIndex = 22;
            this.UiTitleLabel.Text = "Title";
            // 
            // UiTitleTextBox
            // 
            this.UiTitleTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UiTitleTextBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.UiTitleTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Amphi.Properties.Settings.Default, "HeaderTitle", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.UiTitleTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.UiTitleTextBox.Lines = new string[] {
        "Amphi"};
            this.UiTitleTextBox.Location = new System.Drawing.Point(72, 64);
            this.UiTitleTextBox.MaxLength = 32767;
            this.UiTitleTextBox.Name = "UiTitleTextBox";
            this.UiTitleTextBox.PasswordChar = '\0';
            this.UiTitleTextBox.PromptText = "With Placeholder support!";
            this.UiTitleTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.UiTitleTextBox.SelectedText = "";
            this.UiTitleTextBox.Size = new System.Drawing.Size(447, 32);
            this.UiTitleTextBox.TabIndex = 4;
            this.UiTitleTextBox.Text = global::Amphi.Properties.Settings.Default.HeaderTitle;
            this.UiTitleTextBox.UseSelectable = true;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ApplyButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ApplyButton.Location = new System.Drawing.Point(318, 340);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(127, 37);
            this.ApplyButton.TabIndex = 11;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseSelectable = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // ConfigTab
            // 
            this.ConfigTab.AutoScroll = true;
            this.ConfigTab.Controls.Add(this.ConfigurationSettingsInstructionsLabel);
            this.ConfigTab.Controls.Add(this.ImportModesButton);
            this.ConfigTab.Controls.Add(this.ExportModesButton);
            this.ConfigTab.HorizontalScrollbar = true;
            this.ConfigTab.HorizontalScrollbarBarColor = true;
            this.ConfigTab.HorizontalScrollbarHighlightOnWheel = false;
            this.ConfigTab.HorizontalScrollbarSize = 10;
            this.ConfigTab.Location = new System.Drawing.Point(4, 38);
            this.ConfigTab.Name = "ConfigTab";
            this.ConfigTab.Padding = new System.Windows.Forms.Padding(25);
            this.ConfigTab.Size = new System.Drawing.Size(547, 229);
            this.ConfigTab.TabIndex = 3;
            this.ConfigTab.Text = "Configuration";
            this.ConfigTab.VerticalScrollbar = true;
            this.ConfigTab.VerticalScrollbarBarColor = true;
            this.ConfigTab.VerticalScrollbarHighlightOnWheel = false;
            this.ConfigTab.VerticalScrollbarSize = 10;
            // 
            // ExportModesButton
            // 
            this.ExportModesButton.Location = new System.Drawing.Point(3, 103);
            this.ExportModesButton.Name = "ExportModesButton";
            this.ExportModesButton.Size = new System.Drawing.Size(221, 37);
            this.ExportModesButton.TabIndex = 12;
            this.ExportModesButton.Text = "Export Modes";
            this.ExportModesButton.UseSelectable = true;
            this.ExportModesButton.Click += new System.EventHandler(this.ExportModesButton_Click);
            // 
            // ImportModesButton
            // 
            this.ImportModesButton.Location = new System.Drawing.Point(326, 103);
            this.ImportModesButton.Name = "ImportModesButton";
            this.ImportModesButton.Size = new System.Drawing.Size(221, 37);
            this.ImportModesButton.TabIndex = 13;
            this.ImportModesButton.Text = "Import Modes";
            this.ImportModesButton.UseSelectable = true;
            this.ImportModesButton.Click += new System.EventHandler(this.ImportModesButton_Click);
            // 
            // ConfigurationSettingsInstructionsLabel
            // 
            this.ConfigurationSettingsInstructionsLabel.Location = new System.Drawing.Point(3, 12);
            this.ConfigurationSettingsInstructionsLabel.Name = "ConfigurationSettingsInstructionsLabel";
            this.ConfigurationSettingsInstructionsLabel.Size = new System.Drawing.Size(541, 88);
            this.ConfigurationSettingsInstructionsLabel.TabIndex = 14;
            this.ConfigurationSettingsInstructionsLabel.Text = resources.GetString("ConfigurationSettingsInstructionsLabel.Text");
            this.ConfigurationSettingsInstructionsLabel.WrapToLine = true;
            // 
            // ModesAndWidgetsSettingsInstructionsLabel
            // 
            this.ModesAndWidgetsSettingsInstructionsLabel.Location = new System.Drawing.Point(3, 9);
            this.ModesAndWidgetsSettingsInstructionsLabel.Name = "ModesAndWidgetsSettingsInstructionsLabel";
            this.ModesAndWidgetsSettingsInstructionsLabel.Size = new System.Drawing.Size(541, 43);
            this.ModesAndWidgetsSettingsInstructionsLabel.TabIndex = 38;
            this.ModesAndWidgetsSettingsInstructionsLabel.Text = "Manage modes and widgets here. Remember widgets belong to modes, so the widget li" +
    "st will change depending on which mode you select.";
            this.ModesAndWidgetsSettingsInstructionsLabel.WrapToLine = true;
            // 
            // UISettingsInstructionsLabel
            // 
            this.UISettingsInstructionsLabel.Location = new System.Drawing.Point(3, 9);
            this.UISettingsInstructionsLabel.Name = "UISettingsInstructionsLabel";
            this.UISettingsInstructionsLabel.Size = new System.Drawing.Size(541, 43);
            this.UISettingsInstructionsLabel.TabIndex = 39;
            this.UISettingsInstructionsLabel.Text = "Manage dashboard title and application look and feel. Be sure to save your settin" +
    "gs to see any changes in action.";
            this.UISettingsInstructionsLabel.WrapToLine = true;
            // 
            // SettingsView
            // 
            this.AcceptButton = this.SaveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 396);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.SettingsTabs);
            this.Controls.Add(this.SaveButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(601, 396);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(601, 396);
            this.Name = "SettingsView";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.SettingsView_Load);
            this.SettingsTabs.ResumeLayout(false);
            this.ModesTab.ResumeLayout(false);
            this.ModesTab.PerformLayout();
            this.UiTab.ResumeLayout(false);
            this.UiTab.PerformLayout();
            this.ConfigTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton SaveButton;
        private MetroFramework.Controls.MetroTabControl SettingsTabs;
        private MetroFramework.Controls.MetroTabPage UiTab;
        private MetroFramework.Controls.MetroTextBox UiTitleTextBox;
        private MetroFramework.Controls.MetroButton ApplyButton;
        private MetroFramework.Controls.MetroLabel UiThemeLabel;
        private MetroFramework.Controls.MetroLabel UiHighlightLabel;
        private MetroFramework.Controls.MetroLabel UiTitleLabel;
        private MetroFramework.Controls.MetroComboBox UiHighlightComboBox;
        private MetroFramework.Controls.MetroComboBox UiThemeComboBox;
        private MetroFramework.Controls.MetroTabPage ModesTab;
        private MetroFramework.Controls.MetroButton EditWidgetButton;
        private MetroFramework.Controls.MetroButton RemoveWidgetButton;
        private MetroFramework.Controls.MetroButton AddWidgetButton;
        private MetroFramework.Controls.MetroComboBox WidgetListComboBox;
        private MetroFramework.Controls.MetroLabel WidgetsLabel;
        private MetroFramework.Controls.MetroButton EditModeButton;
        private MetroFramework.Controls.MetroButton RemoveModeButton;
        private MetroFramework.Controls.MetroButton AddModeButton;
        private MetroFramework.Controls.MetroComboBox ModeListComboBox;
        private MetroFramework.Controls.MetroLabel ModesLabel;
        private MetroFramework.Controls.MetroTabPage ConfigTab;
        private MetroFramework.Controls.MetroButton ImportModesButton;
        private MetroFramework.Controls.MetroButton ExportModesButton;
        private MetroFramework.Controls.MetroLabel ConfigurationSettingsInstructionsLabel;
        private MetroFramework.Controls.MetroLabel ModesAndWidgetsSettingsInstructionsLabel;
        private MetroFramework.Controls.MetroLabel UISettingsInstructionsLabel;
    }
}