﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amphi.Controllers;
using Amphi.Helpers;
using Amphi.Models;
using MetroFramework.Forms;

namespace Amphi.Views.Settings
{
    public partial class ModeSettingsView : MetroForm
    {
        private Mode activeMode;
        private bool create;

        #region Constructor
        public ModeSettingsView(Mode targetMode, bool createMode = false)
        {
            this.InitializeComponent();
            this.activeMode = targetMode;
            this.create = createMode;
            this.SetTheme();
            this.SetupForm();
        }
        #endregion

        #region Public Form Methods
        public void SetTheme()
        {
            UIHelper.SetTheme(this);
            UIHelper.SetTheme(this.ModeNameLabel);
            UIHelper.SetTheme(this.ModeNameTextBox);
            UIHelper.SetTheme(this.WidgetsLabel);
            UIHelper.SetTheme(this.WidgetListComboBox);
            UIHelper.SetTheme(this.AddWidgetButton);
            UIHelper.SetTheme(this.RemoveWidgetButton);
            UIHelper.SetTheme(this.EditWidgetButton);
            UIHelper.SetTheme(this.CancelModeEditButton);
            UIHelper.SetTheme(this.SaveButton);
        }
        #endregion

        #region Private Form Methods
        private void SetupForm()
        {
            if (this.create)
            {
                this.Text = "Create new Mode";
                this.ModeNameTextBox.Text = string.Empty;
                this.ToggleWidgetControls(false);
            }
            else
            {
                this.Text = this.activeMode.Name + " Settings";
                this.ModeNameTextBox.Text = this.activeMode.Name;
                UIHelper.PopulateWidgetList(this.WidgetListComboBox, this.activeMode);
                this.ToggleWidgetControls(true);
            }
        }

        private void ToggleWidgetControls(bool show)
        {
            this.WidgetListComboBox.Visible = show;
            this.AddWidgetButton.Visible = show;
            this.RemoveWidgetButton.Visible = show;
            this.EditWidgetButton.Visible = show;
            this.WidgetsLabel.Visible = show;
        }

        private bool ApplySettings()
        {
            string applyAction;
            if (this.create)
            {
                applyAction = "update";
            }
            else
            {
                applyAction = "create";
            }

            try
            {
                this.activeMode.Name = this.ModeNameTextBox.Text;
                return true;
            }
            catch (InvalidModeNameException)
            {
                MessageBox.Show(
                    this,
                    "Unable to " + applyAction + " Mode because of an invalid name, please try again",
                    "Invalid Mode Name",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
        }
        #endregion

        #region Private Event Handlers
        private void AddWidgetButton_Click(object sender, EventArgs e)
        {
            ApplicationHelper.AddWidget(this, this.activeMode.Name);
            UIHelper.PopulateWidgetList(this.WidgetListComboBox, this.activeMode);
        }

        private void EditWidgetButton_Click(object sender, EventArgs e)
        {
            string selectedWidget = this.WidgetListComboBox.SelectedItem.ToString();
            ApplicationHelper.EditWidget(this, this.activeMode.Name, selectedWidget);
            UIHelper.PopulateWidgetList(this.WidgetListComboBox, this.activeMode);
        }

        private void RemoveWidgetButton_Click(object sender, EventArgs e)
        {
            string selectedWidget = this.WidgetListComboBox.SelectedItem.ToString();
            ApplicationHelper.DeleteWidget(this, this.activeMode.Name, selectedWidget);
            UIHelper.PopulateWidgetList(this.WidgetListComboBox, this.activeMode);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            bool canClose = this.ApplySettings();
            if (canClose)
            {
                this.Close();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
