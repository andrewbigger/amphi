﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amphi.Controllers;
using Amphi.Helpers;
using Amphi.Models;
using Amphi.Views;
using MetroFramework.Forms;

namespace Amphi
{
    public partial class MainView : MetroForm
    {
        #region Constructor
        public MainView()
        {
            this.InitializeComponent();
            UIHelper.PositionHeader(this);
            this.ApplySettings();
        }
        #endregion

        #region Public Form Methods
        public void SetTheme()
        {
            UIHelper.SetTheme(this);
            UIHelper.SetTheme(this.ModeComboBox);
        }
        #endregion

        #region Private Form Methods
        private void ApplySettings()
        {
            this.SetTitle();
            this.SetTheme();
        }

        private void SetTitle()
        {
            this.Text = Properties.Settings.Default.HeaderTitle;
        }

        private void SetFallbackDefaultMode()
        {
            try
            {
                string defaultMode = ApplicationController.Modes.Index[0].Name;
                Properties.Settings.Default.ActiveMode = defaultMode;
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    this,
                    "Unable to load default mode, please restart Amphi and specify a default mode",
                    "No default mode",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                throw ex;
            }
        }

        private void PopulateModes()
        {
            this.ModeComboBox.Items.Clear();
            foreach (Mode m in ApplicationController.Modes.Index)
            {
                this.ModeComboBox.Items.Add(m.Name);
            }

            UIHelper.SetSelectedItem(this.ModeComboBox, Properties.Settings.Default.ActiveMode);
        }
        #endregion

        #region Private Event Handlers
        private void MainView_Load(object sender, EventArgs e)
        {
            try
            {
                ApplicationController.Load();
                this.PopulateModes();
                UIHelper.ShowActiveMode();
            }
            catch (InvalidCastException ex)
            {
                MessageBox.Show(
                    this,
                    "Unable to read saved state " + ex.Message,
                    "Critical Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                if (ApplicationController.Modes.Index.Count == 0)
                {
                    ApplicationHelper.AddMode(this);
                }
                else
                {
                    Properties.Settings.Default.ActiveMode = ApplicationController.Modes.Index[0].Name;
                }

                try
                {
                    this.SetFallbackDefaultMode();
                    this.PopulateModes();
                    UIHelper.ShowActiveMode();
                }
                catch (Exception)
                {
                    Application.Exit();
                }
            }
        }

        private void HeaderView_Move(object sender, EventArgs e)
        {
            UIHelper.PositionHeader(this);
        }

        private void HeaderView_TitleChange(object sender, EventArgs e)
        {
            this.SetTitle();
        }

        private void ModeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.ActiveMode = this.ModeComboBox.SelectedItem.ToString();
            UIHelper.ShowActiveMode();
        }

        private void AddWidgetToActiveModeButton_Click(object sender, EventArgs e)
        {
            ApplicationHelper.AddWidget(this, Properties.Settings.Default.ActiveMode);
        }

        private void SettingsButton_Click(object sender, EventArgs e)
        {
            SettingsView settings = new SettingsView();
            settings.ShowDialog();
            this.ApplySettings();
            this.PopulateModes();
            this.Refresh();
        }

        private void HelpButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Properties.Settings.Default.WikiUrl);
        }

        private void AboutButton_Click(object sender, EventArgs e)
        {
            AboutDialog about = new AboutDialog();
            about.ShowDialog();
        }

        #endregion
    }
}
