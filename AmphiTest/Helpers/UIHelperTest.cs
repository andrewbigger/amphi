﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MetroFramework;
using MetroFramework.Controls;
using MetroFramework.Forms;

using Amphi.Helpers;

namespace AmphiTest
{
    #region Header Helpers
    [TestClass]
    public class PositionHeader
    {
        private MetroForm Subject;

        [TestInitialize]
        public void Setup()
        {
            Subject = new MetroForm();
            UIHelper.PositionHeader(Subject);
        }

        [TestMethod]
        public void Form_Start_Position_Is_Set_Manual()
        {
            Assert.AreEqual(Subject.StartPosition, FormStartPosition.Manual);
        }

        [TestMethod]
        public void Form_Width_Is_Set_To_Primary_Screen_Size()
        {
            int Expected_FormWidth = Screen.PrimaryScreen.Bounds.Width;
            Assert.AreEqual(Subject.Width, Expected_FormWidth);
        }

        [TestMethod]
        public void Form_Location_Is_Set_Correctly()
        {
            Point Expected_Location = new Point(0, 0);
            Assert.AreEqual(Subject.Location, Expected_Location);
        }

        [TestMethod]
        public void Form_Height_Is_Set_Correctly()
        {
            Assert.AreEqual(Subject.Height, UIHelper.Top);
        }
    }
    #endregion

    #region Application Style Helpers
    [TestClass]
    public class SetTheme
    {
        private MetroForm frm;
        private MetroButton btn;
        private MetroComboBox cmb;
        private MetroTextBox txt;
        private MetroLabel lbl;
        private MetroTabControl tabs;
        private MetroTabPage tab;
        private MetroTile tile;
        private MetroProgressBar bar;
        private MetroPanel pnl;

        [TestInitialize]
        public void Setup()
        {
            MetroColorStyle testStyle = MetroColorStyle.Brown;
            MetroThemeStyle testTheme = MetroThemeStyle.Dark;

            #region Test Objects
            frm = new MetroForm();
            frm.Style = MetroColorStyle.Brown;
            frm.Theme = MetroThemeStyle.Dark;

            btn = new MetroButton();
            btn.Style = testStyle;
            btn.Theme = testTheme;

            cmb = new MetroComboBox();
            cmb.Style = testStyle;
            cmb.Theme = testTheme;

            lbl = new MetroLabel();
            lbl.Style = testStyle;
            lbl.Theme = testTheme;

            txt = new MetroTextBox();
            txt.Style = testStyle;
            txt.Theme = testTheme;

            tabs = new MetroTabControl();
            tabs.Style = testStyle;
            tabs.Theme = testTheme;

            tab = new MetroTabPage();
            tab.Style = testStyle;
            tab.Theme = testTheme;

            tile = new MetroTile();
            tile.Style = testStyle;
            tile.Theme = testTheme;

            bar = new MetroProgressBar();
            bar.Style = testStyle;
            bar.Theme = testTheme;

            pnl = new MetroPanel();
            pnl.Style = testStyle;
            pnl.Theme = testTheme;
            #endregion
        }

        [TestMethod]
        public void Form_Style_Is_Set_To_Default()
        {
            UIHelper.SetTheme(frm);
            Assert.AreNotEqual(frm.Style, MetroColorStyle.Brown);
        }

        [TestMethod]
        public void Form_Theme_Is_Set_To_Default()
        {
            UIHelper.SetTheme(frm);
            Assert.AreNotEqual(frm.Style, MetroThemeStyle.Dark);
        }

        [TestMethod]
        public void Can_Apply_Theme_To_Metro_Button()
        {
            UIHelper.SetTheme(btn);
            Assert.IsTrue(btn.Theme == UIHelper.Theme);
        }

        [TestMethod]
        public void Can_Apply_Colour_To_Metro_Button()
        {
            UIHelper.SetTheme(btn);
            Assert.IsTrue(btn.Style == UIHelper.Colour);
        }

        [TestMethod]
        public void Can_Apply_Theme_To_Metro_ComboBox()
        {
            UIHelper.SetTheme(cmb);
            Assert.IsTrue(cmb.Theme == UIHelper.Theme);
        }

        [TestMethod]
        public void Can_Apply_Colour_To_Metro_ComboBox()
        {
            UIHelper.SetTheme(cmb);
            Assert.IsTrue(cmb.Style == UIHelper.Colour);
        }

        [TestMethod]
        public void Can_Apply_Theme_To_Metro_Label()
        {
            UIHelper.SetTheme(lbl);
            Assert.IsTrue(lbl.Theme == UIHelper.Theme);
        }

        [TestMethod]
        public void Can_Apply_Colour_To_Metro_Label()
        {
            UIHelper.SetTheme(lbl);
            Assert.IsTrue(lbl.Style == UIHelper.Colour);
        }

        [TestMethod]
        public void Can_Apply_Theme_To_Metro_TextBox()
        {
            UIHelper.SetTheme(txt);
            Assert.IsTrue(txt.Theme == UIHelper.Theme);
        }

        [TestMethod]
        public void Can_Apply_Colour_To_Metro_TextBox()
        {
            UIHelper.SetTheme(txt);
            Assert.IsTrue(txt.Style == UIHelper.Colour);
        }

        [TestMethod]
        public void Can_Apply_Theme_To_Metro_TabControl()
        {
            UIHelper.SetTheme(tabs);
            Assert.IsTrue(tabs.Theme == UIHelper.Theme);
        }

        [TestMethod]
        public void Can_Apply_Colour_To_Metro_TabControl()
        {
            UIHelper.SetTheme(tabs);
            Assert.IsTrue(tabs.Style == UIHelper.Colour);
        }

        [TestMethod]
        public void Can_Apply_Theme_To_Metro_TabPage()
        {
            UIHelper.SetTheme(tab);
            Assert.IsTrue(tab.Theme == UIHelper.Theme);
        }

        [TestMethod]
        public void Can_Apply_Colour_To_Metro_TabPage()
        {
            UIHelper.SetTheme(tab);
            Assert.IsTrue(tab.Style == UIHelper.Colour);
        }

        [TestMethod]
        public void Can_Apply_Theme_To_Metro_Tile()
        {
            UIHelper.SetTheme(tile);
            Assert.IsTrue(tile.Theme == UIHelper.Theme);
        }

        [TestMethod]
        public void Can_Apply_Colour_To_Metro_Tile()
        {
            UIHelper.SetTheme(tile);
            Assert.IsTrue(tile.Style == UIHelper.Colour);
        }

        [TestMethod]
        public void Can_Apply_Theme_To_Metro_ProgressBar()
        {
            UIHelper.SetTheme(bar);
            Assert.IsTrue(bar.Theme == UIHelper.Theme);
        }

        [TestMethod]
        public void Can_Apply_Colour_To_Metro_Panel()
        {
            UIHelper.SetTheme(pnl);
            Assert.IsTrue(pnl.Style == UIHelper.Colour);
        }

        [TestMethod]
        public void Can_Apply_Theme_To_Metro_Panel()
        {
            UIHelper.SetTheme(pnl);
            Assert.IsTrue(pnl.Theme == UIHelper.Theme);
        }
    }

    [TestClass]
    public class StyleOf
    {
        private String opts = @"Black, Green, Blue, Orange, 
            Brown, Lime, Magenta, Pink,  Purple, Red, Silver, Teal, 
            White, Yellow";
        private String[] ColorOptionsList;

        [TestInitialize]
        public void Setup()
        {
            ColorOptionsList = opts.Split(',');
        }

        [TestMethod]
        public void Can_Return_Default_Colour_Option()
        {
            Assert.AreEqual(UIHelper.StyleOf("Bogus"), MetroColorStyle.Blue);
        }

        [TestMethod]
        public void Can_Return_All_Colour_Options()
        {
            foreach (String s in ColorOptionsList) 
            {
                MetroColorStyle Result = UIHelper.StyleOf(s.Trim());
                String ExpectedResult = s.Trim();
                String ActualResult = Result.ToString();
                Assert.AreEqual(ActualResult, ExpectedResult); 
            }
        }
    }

    [TestClass]
    public class ThemeOf
    {

        [TestMethod]
        public void Can_Return_Default_Light_Theme()
        {
            Assert.AreEqual(UIHelper.ThemeOf("Bogus"), MetroThemeStyle.Light);
        }

        [TestMethod]
        public void Can_Return_Light_Theme()
        {
            Assert.AreEqual(UIHelper.ThemeOf("Light"), MetroThemeStyle.Light);
        }

        [TestMethod]
        public void Can_Return_Dark_Theme()
        {
            Assert.AreEqual(UIHelper.ThemeOf("Dark"), MetroThemeStyle.Dark);
        }
    }
    #endregion

    #region Widget Helpers

    #region Form Modes
    [TestClass]
    public class ToggleLockOn
    {
        private Form Subject;
        private int OrigWidth = 200;
        private int OrigHeight = 300;

        [TestInitialize]
        public void Setup()
        {
            Subject = new Form();
            Subject.FormBorderStyle = FormBorderStyle.SizableToolWindow;
            Subject.Size = new Size(OrigWidth, OrigHeight);
            UIHelper.ToggleLock(Subject, false);
        }

        [TestMethod]
        public void Set_Form_Border_Style_To_None()
        {
            Assert.AreEqual(Subject.FormBorderStyle, FormBorderStyle.None);
        }

        [TestMethod]
        public void Maintains_Form_Width_After_ToggleLock_On()
        {
            Assert.AreEqual(Subject.Width, OrigWidth);
        }

        [TestMethod]
        public void Maintains_Form_Height_After_ToggleLock_On()
        {
            Assert.AreEqual(Subject.Height, OrigHeight);
        }
    }

    [TestClass]
    public class ToggleLockOff
    {
        private Form Subject;
        private int OrigWidth = 200;
        private int OrigHeight = 300;

        [TestInitialize]
        public void Setup()
        {
            Subject = new Form();
            Subject.FormBorderStyle = FormBorderStyle.None;
            Subject.Size = new Size(OrigWidth, OrigHeight);
            UIHelper.ToggleLock(Subject, true);
        }

        [TestMethod]
        public void Set_Form_Border_Style_SizableToolWindow()
        {
            Assert.AreEqual(Subject.FormBorderStyle, FormBorderStyle.SizableToolWindow);
        }

        [TestMethod]
        public void Maintains_Form_Width_After_ToggleLock_Off()
        {
            Assert.AreEqual(Subject.Width, OrigWidth);
        }

        [TestMethod]
        public void Maintains_Form_Height_After_ToggleLock_Off()
        {
            Assert.AreEqual(Subject.Height, OrigHeight);
        }
    }

    [TestClass]
    public class ToggleFullScreenOn
    {
        private Form Subject;

        [TestInitialize]
        public void Setup()
        {
            Subject = new Form();
            Subject.WindowState = FormWindowState.Normal;
            UIHelper.ToggleFullScreen(Subject, false);
        }

        [TestMethod]
        public void Set_Form_WindowState_To_Maximized()
        {
            Assert.AreEqual(Subject.WindowState, FormWindowState.Maximized);
        }

        [TestMethod]
        public void Locks_Form_After_ToggleFullscreen_On()
        {
            Assert.AreEqual(Subject.FormBorderStyle, FormBorderStyle.None);
        }
    }

    [TestClass]
    public class ToggleFullScreenOff
    {
        private Form Subject;

        [TestInitialize]
        public void Setup()
        {
            Subject = new Form();
            Subject.WindowState = FormWindowState.Maximized;
            UIHelper.ToggleFullScreen(Subject, true);
        }

        [TestMethod]
        public void Set_Form_WindowState_To_Normal()
        {
            Assert.AreEqual(Subject.WindowState, FormWindowState.Normal);
        }

        [TestMethod]
        public void Locks_Form_After_ToggleFullscreen_Off()
        {
            Assert.AreEqual(Subject.FormBorderStyle, FormBorderStyle.SizableToolWindow);
        }
    }
    #endregion

    #region Form Size Helpers
    [TestClass]
    public class WindowBoundsDefaultBoundsPrimaryScreen
    {
        private Size Result;
        private int expectedWidth;
        private int expectedHeight;

        [TestInitialize]
        public void Setup()
        {
            expectedWidth = Screen.PrimaryScreen.Bounds.Width/2;
            expectedHeight = (Screen.PrimaryScreen.Bounds.Height - UIHelper.Top) / 2;
            Result = UIHelper.WindowBounds(Screen.PrimaryScreen);
        }

        [TestMethod]
        public void Returns_Half_Width_Of_Screen_By_Default()
        {
            Assert.AreEqual(Result.Width, expectedWidth);
        }

        [TestMethod]
        public void Returns_Half_Screen_Height_By_Default()
        {
            Assert.AreEqual(Result.Height, expectedHeight);
        }
    }

    [TestClass]
    public class WindowBoundsCustomBoundsPrimaryScreen
    {
        private int expectedWidth;
        private int expectedHeight;

        [TestInitialize]
        public void Setup()
        {
            expectedWidth = Screen.PrimaryScreen.Bounds.Width;
            expectedHeight = Screen.PrimaryScreen.Bounds.Height - UIHelper.Top;
        }

        [TestMethod]
        public void Returns_Third_Of_Screen_Width_Given_Params()
        {
            expectedWidth = Screen.PrimaryScreen.Bounds.Width / 3;
            Assert.AreEqual(UIHelper.WindowBounds(Screen.PrimaryScreen, 3).Width, expectedWidth);
        }

        [TestMethod]
        public void Returns_Full_Screen_Width_Given_Params()
        {
            expectedWidth = Screen.PrimaryScreen.Bounds.Width;
            Assert.AreEqual(UIHelper.WindowBounds(Screen.PrimaryScreen, 1).Width, expectedWidth);
        }

        [TestMethod]
        public void Returns_Third_Of_Screen_Height_Given_Params()
        {
            expectedHeight = (Screen.PrimaryScreen.Bounds.Height - UIHelper.Top) / 3;
            Assert.AreEqual(UIHelper.WindowBounds(Screen.PrimaryScreen, 1, 3).Height, expectedHeight);
        }

        [TestMethod]
        public void Returns_Full_Screen_Height_Given_Params()
        {
            expectedHeight = (Screen.PrimaryScreen.Bounds.Height - UIHelper.Top) / 1;
            int Result = UIHelper.WindowBounds(Screen.PrimaryScreen, 1, 1).Height;
            Assert.AreEqual(Result, expectedHeight);
        }
    }

    [TestClass]
    public class SizeWindow
    {
        private Form Subject;
        private Size Target;

        [TestInitialize]
        public void Setup()
        {
            Subject = new Form();
            Target = new Size(300, 400);
            UIHelper.SizeWindow(Subject, Target);
        }

        [TestMethod]
        public void Sets_Width_Of_Form()
        {
            Assert.AreEqual(Subject.Width, 300);
        }

        [TestMethod]
        public void Sets_Height_Of_Form()
        {
            Assert.AreEqual(Subject.Height, 400);
        }
    }
    #endregion

    #region Form Position Helpers
    [TestClass]
    public class PositionWindow
    {
        private Form Subject;
        private Point Target;

        [TestInitialize]
        public void Setup()
        {
            Subject = new Form();
            Target = new Point(30, 40);
            UIHelper.PositionWindow(Subject, Target);
        }

        [TestMethod]
        public void Sets_X_Co_Ordinate()
        {
            Assert.AreEqual(Subject.Location.X, 30);
        }

        [TestMethod]
        public void Sets_Y_Co_Ordinate()
        {
            Assert.AreEqual(Subject.Location.Y, 40);
        }
    }

    [TestClass]
    public class WindowLocationHalfScreen
    {
        private Form Subject;

        private int yTop;
        private int yBottom;

        private int xLeft;
        private int xRight;

        [TestInitialize]
        public void Setup()
        {
            Subject = new Form();
            Size halfScreen = UIHelper.WindowBounds(Screen.PrimaryScreen);
            UIHelper.SizeWindow(Subject, halfScreen);
            yTop = UIHelper.Top;
            yBottom = Screen.PrimaryScreen.Bounds.Height - UIHelper.Top - Subject.Height;

            xLeft = 0;
            xRight = Screen.PrimaryScreen.Bounds.Width - Subject.Width;
        }

        [TestMethod]
        public void Sets_Top_Left_Alignment_By_Default()
        {
            Point expectedPoint = new Point(xLeft, yTop);
            Point actualPoint = UIHelper.WindowLocation(Screen.PrimaryScreen, Subject.Size);
            Assert.AreEqual(actualPoint, expectedPoint);
        }

        [TestMethod]
        public void Can_Set_Bottom_Left()
        {
            Point expectedPoint = new Point(xLeft, yBottom);
            Point actualPoint = UIHelper.WindowLocation(Screen.PrimaryScreen, Subject.Size, "bottom");
            Assert.AreEqual(expectedPoint, actualPoint);
        }

        [TestMethod]
        public void Can_Set_Top_Right_Alignment()
        {
            Point expectedPoint = new Point(xRight, yTop);
            Point actualPoint = UIHelper.WindowLocation(Screen.PrimaryScreen, Subject.Size, "top", "right");
            Assert.AreEqual(actualPoint, expectedPoint);
        }

        [TestMethod]
        public void Can_Set_Bottom_Right()
        {
            Point expectedPoint = new Point(xRight, yBottom);
            Point actualPoint = UIHelper.WindowLocation(Screen.PrimaryScreen, Subject.Size, "bottom", "right");
            Assert.AreEqual(actualPoint, expectedPoint);
        }
    }

    [TestClass]
    public class WindowLocationThirdScreen
    {
        private Form Subject;

        private int yTop;
        private int yBottom;

        private int xCentre;
        private int xRight;

        [TestInitialize]
        public void Setup()
        {
            Subject = new Form();
            Size thirdScreen = UIHelper.WindowBounds(Screen.PrimaryScreen, 3);
            UIHelper.SizeWindow(Subject, thirdScreen);

            yTop = UIHelper.Top;
            yBottom = Screen.PrimaryScreen.Bounds.Height - UIHelper.Top - Subject.Height;

            xCentre = (Screen.PrimaryScreen.Bounds.Width / 2) - (Subject.Width / 2);
            xRight = Screen.PrimaryScreen.Bounds.Width - Subject.Width;
        }

        [TestMethod]
        public void Can_Set_Top_Centre()
        {
            Point expectedPoint = new Point(xCentre, yTop);
            Point actualPoint = UIHelper.WindowLocation(Screen.PrimaryScreen, Subject.Size, "top", "centre");
            Assert.AreEqual(actualPoint, expectedPoint);
        }

        [TestMethod]
        public void Can_Set_Bottom_Centre()
        {
            Point expectedPoint = new Point(xCentre, yBottom);
            Point actualPoint = UIHelper.WindowLocation(Screen.PrimaryScreen, Subject.Size, "bottom", "centre");
            Assert.AreEqual(expectedPoint, actualPoint);
        }
    }
    #endregion
    
    #endregion
}
