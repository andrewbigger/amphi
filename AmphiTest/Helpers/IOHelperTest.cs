﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Amphi.Helpers;
using System.Windows.Forms;

namespace AmphiTest.Helpers
{
    [TestClass]
    public class WriteBinary
    {
        MemoryStream subject;
        string resultPath = Path.GetTempFileName();

        [TestInitialize]
        public void Setup()
        {
            subject = IOHelper.SerializeBinary("Some Object");
            IOHelper.Write(resultPath, subject);
        }

        [TestMethod]
        public void Can_Write_Binary_File_From_Object()
        {
            FileInfo info = new FileInfo(resultPath);
            Assert.AreEqual(info.Length, 35);
        }

        [TestCleanup]
        public void Cleanup()
        {
            File.Delete(resultPath);
        }
    }

    [TestClass]
    public class ReadBinary
    {
        MemoryStream subject;
        string readPath = Path.GetTempFileName();

        [TestInitialize]
        public void Setup()
        {
            subject = IOHelper.SerializeBinary("An object deserialized is a happy object");
            IOHelper.Write(readPath, subject);
        }

        [TestMethod]
        public void Can_Read_Binary_File_To_Object()
        {
            MemoryStream content = IOHelper.Read(readPath);
            Object deserialized = IOHelper.DeserializeBinary(content);
            Assert.AreEqual((string)deserialized, "An object deserialized is a happy object");
        }

        [TestCleanup]
        public void Cleanup()
        {
            File.Delete(readPath);
        }
    }

    [TestClass]
    public class DeserializeAndDeserializeBinary
    {
        [TestMethod]
        public void Can_Deserialize_Object_Without_Error()
        {
            MemoryStream serialized = IOHelper.SerializeBinary("Something for deserial");
            Assert.AreEqual((string)IOHelper.DeserializeBinary(serialized), "Something for deserial");
        }
    }
}
