﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Amphi.Controllers;
using Amphi.Models;
using Amphi.Helpers;

namespace AmphiTest.Controllers
{
    #region Index
    [TestClass]
    public class ModeIndex
    {
        ModeController subject = new ModeController();

        [TestInitialize]
        public void Setup()
        {
            subject.Create("New Mode");
            subject.Create("New Mode 2");
        }

        [TestMethod]
        public void Has_Index_Of_Modes()
        {
            Assert.AreEqual(subject.Index.Count, 2);
        }
    }
    #endregion

    #region Create
    [TestClass]
    public class CreateMode
    {
        ModeController subject = new ModeController();

        [TestMethod]
        public void Can_Create_Mode()
        {
            Point location = new Point(0, 0);
            subject.Create("New Mode 3");
            Assert.AreEqual(subject.Index[0].Name, "New Mode 3");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidModeNameException), "Invalid Name")]
        public void Does_Not_Create_Invalid_Mode()
        {
            Point location = new Point(0, 0);
            subject.Create(" ");
            Assert.AreEqual(subject.Index.Count, 0);
        }
    }
    #endregion

    #region Find
    [TestClass]
    public class FindMode
    {
        ModeController subject = new ModeController();

        [TestInitialize]
        public void Setup()
        {
            subject.Create("Some Mode");
            subject.Create("Completely Different Mode");
        }

        [TestMethod]
        public void Can_Find_Mode()
        {
            List<Mode> results = subject.Find("Some Mode");
            Assert.AreEqual(results.Count, 1);
        }
    }
    #endregion

    #region Update
    [TestClass]
    public class UpdateMode
    {
        ModeController subject = new ModeController();

        [TestInitialize]
        public void Setup()
        {
            subject.Create("An Old Mode");
            subject.Create("Another Mode");
            subject.Create("A Similar Mode");

            Mode targetMode = subject.Find("An Old Mode")[0];
            Mode newMode = new Mode("New Mode");
            subject.Update(targetMode, newMode);
        }

        [TestMethod]
        public void New_Mode_Made_It_Into_Index_After_Update()
        {
            Assert.AreEqual(subject.Find("New Mode").Count, 1);
        }

        [TestMethod]
        public void Old_Mode_Is_No_More_After_Update()
        {
            Assert.AreEqual(subject.Find("An Old Mode").Count, 0);
        }

        [TestMethod]
        public void Index_Is_As_Long_As_Can_Be_Expected_After_Update_Of_Mode()
        {
            Assert.AreEqual(subject.Index.Count, 3);
        }
    }
    #endregion

    #region Destroy
    [TestClass]
    public class DestroyMode
    {
        ModeController subject = new ModeController();

        [TestInitialize]
        public void Setup()
        {
            subject.Create("An Old Mode");
            subject.Create("Another Mode");

            Mode targetMode = subject.Find("An Old Mode")[0];
            subject.Destroy(targetMode);
        }

        [TestMethod]
        public void Old_Mode_Is_No_More_After_Destroy()
        {
            Assert.AreEqual(subject.Find("An Old Mode").Count, 0);
        }

        [TestMethod]
        public void Index_Is_As_Long_As_Can_Be_Expected_After_Destroy_Of_Mode()
        {
            Assert.AreEqual(subject.Index.Count, 1);
        }
    }


    public class DestroyModeByString
    {
        ModeController subject = new ModeController();

        [TestInitialize]
        public void Setup()
        {
            subject.Create("An Old Mode");
            subject.Create("Another Mode");

            subject.Destroy("An Old Mode");
        }

        [TestMethod]
        public void Old_Mode_Is_No_More_After_Destroy_By_String()
        {
            Assert.AreEqual(subject.Find("An Old Mode").Count, 0);
        }

        [TestMethod]
        public void Index_Is_As_Long_As_Can_Be_Expected_After_Destroy_Of_Mode_By_String()
        {
            Assert.AreEqual(subject.Index.Count, 1);
        }
    }
    [TestClass]
    public class DestroyAllMode
    {
        ModeController subject = new ModeController();

        [TestInitialize]
        public void Setup()
        {
            subject.Create("An Old Mode");
            subject.Create("Another Mode");
        }

        [TestMethod]
        public void Index_Is_As_Long_As_Can_Be_Expected_After_Destroy_All_Of_Mode()
        {
            subject.DestroyAll();
            Assert.AreEqual(subject.Index.Count, 0);
        }
    }
    #endregion

}
