﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Amphi.Controllers;
using Amphi.Helpers;
using Amphi.Models;
using Amphi.Views;

namespace AmphiTest.Controllers
{
    #region Properties
    [TestClass]
    public class SpecialFolderProperty
    {

        [TestMethod]
        public void Returns_Mode_Data_Path()
        {
            string expectedPath = Environment.GetFolderPath(
                Environment.SpecialFolder.ApplicationData) + 
                "\\Amphi Dashboard\\modes.bin";
            Assert.AreEqual(ApplicationController.ModeDataPath, expectedPath);
        }

    }
    #endregion

    #region Create
    [TestClass]
    public class CreateWidgetInMode
    {

        [TestInitialize]
        public void Setup()
        {
            ApplicationController.Modes.Create("Test Mode");
        }

        [TestMethod]
        public void Can_Create_Test_Widget_Within_Mode()
        {
            ApplicationController.Create("Test Mode", "Test Widget", "http://test.com");
            Mode testMode = ApplicationController.Modes.Find("Test Mode")[0];
            Assert.IsTrue(testMode.Widgets.Find("Test Widget").Count == 1);
        }

        [TestMethod]
        [ExpectedException(typeof(MissingItemException), "Cannot find Mode")]
        public void Does_Not_Create_Widget_Given_Invalid_Mode_Params()
        {
            ApplicationController.Create("An Non Existent Mode", "Test Widget 2", "http://test2.com");
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidWidgetNameException), "Invalid name")]
        public void Does_Not_Create_Widget_Given_Valid_Mode_Params_And_Invalid_Widget_Params()
        {
            ApplicationController.Create("Test Mode", " ", "http://test2.com");
            Assert.IsTrue(ApplicationController.Modes.Find("Test Mode")[0].Widgets.Index.Count == 0);
        }

        [TestCleanup]
        public void Tidy()
        {
            ApplicationController.Modes.DestroyAll();
            ApplicationController.WidgetViews.DestroyAll();
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }
    }

    [TestClass]
    public class CreateStubMode
    {

        [TestMethod]
        public void Can_Create_Stub_Widget()
        {
            ApplicationController.CreateStubMode();
            Assert.IsTrue(ApplicationController.Modes.Find("stub").Count == 1);
        }

        [TestCleanup]
        public void Tidy()
        {
            ApplicationController.Modes.DestroyAll();
            ApplicationController.WidgetViews.DestroyAll();
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }
    }

    [TestClass]
    public class CreateStubWidget
    {

        [TestInitialize]
        public void Setup()
        {
            ApplicationController.Modes.Create("Test Mode");
        }

        [TestMethod]
        public void Can_Create_Stub_Widget()
        {
            ApplicationController.CreateStubWidget("Test Mode");
            Mode testMode = ApplicationController.Modes.Find("Test Mode")[0];
            Assert.IsTrue(testMode.Widgets.Find("stub").Count == 1);
        }

        [TestMethod]
        [ExpectedException(typeof(MissingItemException), "Cannot find Mode")]
        public void Does_Not_Create_Widget_Given_Invalid_Mode_Params()
        {
            ApplicationController.CreateStubWidget("An Non Existent Mode");
        }

        [TestCleanup]
        public void Tidy()
        {
            ApplicationController.Modes.DestroyAll();
            ApplicationController.WidgetViews.DestroyAll();
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }
    }
    #endregion

    #region Destroy
    [TestClass]
    public class DestroyModeFromAppController
    {

        private void SetupModeForDestruction()
        {
            ApplicationController.Modes.Create("Test Mode");
        }

        private void SetupWidgetsForDestruction()
        {
            ApplicationController.Create("Test Mode", "Test", "http://widget-url.com");
            ApplicationController.Create("Test Mode", "Test2", "http://widget-url2.com");
            Assert.IsTrue(ApplicationController.WidgetViews.Index.Count == 2);
        }

        [TestMethod]
        public void Can_Destroy_Mode()
        {
            this.SetupModeForDestruction();
            ApplicationController.Destroy("Test Mode");
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }

        [TestMethod]
        [ExpectedException(typeof(MissingItemException), "Cannot find Mode")]
        public void When_Given_Bad_Parameters()
        {
            ApplicationController.Destroy("Non Existent Mode");
        }

        [TestMethod]
        public void Destroys_WidgetViews()
        {
            this.SetupModeForDestruction();
            this.SetupWidgetsForDestruction();
            ApplicationController.Destroy("Test Mode");
            Assert.IsTrue(ApplicationController.WidgetViews.Index.Count == 0);  
        }

        [TestCleanup]
        public void Tidy()
        {
            ApplicationController.Modes.DestroyAll();
            ApplicationController.WidgetViews.DestroyAll();
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }
    }

    [TestClass]
    public class DestroyWidgetFromAppController
    {

        private void SetupModeForDestruction()
        {
            ApplicationController.Modes.Create("Test Mode");
        }

        private void SetupWidgetsForDestruction()
        {
            ApplicationController.Create("Test Mode", "Test", "http://widget-url.com");
            ApplicationController.Create("Test Mode", "Test2", "http://widget-url2.com");
            Assert.IsTrue(ApplicationController.WidgetViews.Index.Count == 2);
        }

        [TestMethod]
        public void Can_Destroy_Widget()
        {
            this.SetupModeForDestruction();
            this.SetupWidgetsForDestruction();
            ApplicationController.Destroy("Test Mode", "Test");
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 1);
        }

        [TestMethod]
        [ExpectedException(typeof(MissingItemException), "Cannot find Mode")]
        public void Throws_Error_When_Cannot_Find_Mode()
        {
            ApplicationController.Destroy("An Non Existent Mode", "Test2");
        }

        [TestMethod]
        [ExpectedException(typeof(MissingItemException), "Cannot find Widget")]
        public void Throws_Error_When_Cannot_Find_Widget()
        {
            ApplicationController.Destroy("Test Mode", "Test 3");
        }

        [TestCleanup]
        public void Tidy()
        {
            ApplicationController.Modes.DestroyAll();
            ApplicationController.WidgetViews.DestroyAll();
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }
    }

    [TestClass]
    public class DestroyStubMode
    {

        [TestMethod]
        public void Can_Create_Stub_Widget()
        {
            ApplicationController.CreateStubMode();
            ApplicationController.DestroyStubMode();
            Assert.IsTrue(ApplicationController.Modes.Find("stub").Count == 0);
        }

        [TestCleanup]
        public void Tidy()
        {
            ApplicationController.Modes.DestroyAll();
            ApplicationController.WidgetViews.DestroyAll();
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }
    }

    [TestClass]
    public class DestroyStubWidget
    {

        [TestInitialize]
        public void Setup()
        {
            ApplicationController.Modes.Create("Test Mode");
        }

        [TestMethod]
        public void Can_Destroy_Stub_Widget()
        {
            ApplicationController.CreateStubWidget("Test Mode");
            ApplicationController.DestroyStubWidget("Test Mode");
            Mode testMode = ApplicationController.Modes.Find("Test Mode")[0];
            Assert.IsTrue(testMode.Widgets.Find("stub").Count == 0);
        }

        [TestCleanup]
        public void Tidy()
        {
            ApplicationController.Modes.DestroyAll();
            ApplicationController.WidgetViews.DestroyAll();
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }
    }

    #endregion

    # region Find
    [TestClass]
    public class FindWidgetGivenMode
    {

        private void SetupModes()
        {
            ApplicationController.Modes.Create("Test Mode");
            ApplicationController.Modes.Create("Another Test Mode");
        }

        private void SetupWidgets()
        {
            ApplicationController.Create("Test Mode", "Test", "http://widget-url.com");
            ApplicationController.Create("Test Mode", "Test2", "http://widget-url2.com");
            ApplicationController.Create("Another Test Mode", "Test", "http://widget-urlz.com");
            ApplicationController.Create("Another Test Mode", "Test2", "http://widget-urlz2.com");
            Assert.IsTrue(ApplicationController.WidgetViews.Index.Count == 4);
        }

        [TestMethod]
        public void Can_Find_Widget()
        {
            this.SetupModes();
            this.SetupWidgets();
            Assert.AreEqual(ApplicationController.Find("Test Mode", "Test").GetUrl, "http://widget-url.com");
            this.Tidy();
        }

        [TestMethod]
        [ExpectedException(typeof(MissingItemException), "Cannot find Mode")]
        public void Throws_Error_When_Cannot_Find_Mode_On_Find()
        {
            ApplicationController.Find("An Non Existent Mode", "Test2");
        }

        [TestMethod]
        [ExpectedException(typeof(MissingItemException), "Cannot find Widget")]
        public void Throws_Error_When_Cannot_Find_Widget()
        {
            ApplicationController.Find("Test Mode", "Test 3");
        }

        private void Tidy()
        {
            ApplicationController.Modes.DestroyAll();
            ApplicationController.WidgetViews.DestroyAll();
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }
     }

    [TestClass]
    public class FindWidgetViewGivenModeAndWidget
    {

        private void SetupModes()
        {
            ApplicationController.Modes.Create("Test Mode");
            ApplicationController.Modes.Create("Another Test Mode");
        }

        private void SetupWidgets()
        {
            ApplicationController.Create("Test Mode", "Test", "http://widget-url.com");
            ApplicationController.Create("Test Mode", "Test2", "http://widget-url2.com");
            ApplicationController.Create("Another Test Mode", "Test", "http://widget-urlz.com");
            ApplicationController.Create("Another Test Mode", "Test2", "http://widget-urlz2.com");
            Assert.IsTrue(ApplicationController.WidgetViews.Index.Count == 4);
        }

        [TestMethod]
        public void Can_Find_WidgetView()
        {
            this.SetupModes();
            this.SetupWidgets();
            HtmlWidgetView result = ApplicationController.FindWidgetView("Test Mode", "Test");
            Assert.IsNotNull(result);
            this.Tidy();
        }

        [TestMethod]
        [ExpectedException(typeof(MissingItemException), "Cannot find Mode")]
        public void Throws_Error_When_Cannot_Find_Mode_On_FindWidgetView()
        {
            ApplicationController.FindWidgetView("An Non Existent Mode", "Test2");
        }

        [TestMethod]
        [ExpectedException(typeof(MissingItemException), "Cannot find Widget")]
        public void Throws_Error_When_Cannot_Find_WidgetView()
        {
            ApplicationController.FindWidgetView("Test Mode", "Test 3");
        }

        private void Tidy()
        {
            ApplicationController.Modes.DestroyAll();
            ApplicationController.WidgetViews.DestroyAll();
            Assert.IsTrue(ApplicationController.Modes.Index.Count == 0);
        }
    }
    #endregion
}
