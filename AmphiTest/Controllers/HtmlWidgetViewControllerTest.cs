﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Amphi.Controllers;
using Amphi.Models;
using Amphi.Views;
using Amphi.Helpers;

namespace AmphiTest.Controllers
{
    #region Index
    [TestClass]
    public class WidgetViewIndex
    {
        HtmlWidgetViewController subject = new HtmlWidgetViewController();

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            subject.Create(new HtmlWidget("New Widget 1", "http://widget-1.com", location));
            subject.Create(new HtmlWidget("New Widget 2", "http://widget-2.com", location));
        }

        [TestMethod]
        public void Has_Index_Of_Widget_Views()
        {
            Assert.AreEqual(subject.Index.Count, 2);
        }
    }
    #endregion

    #region Create
    [TestClass]
    public class CreateWidgetView
    {
        HtmlWidgetViewController subject = new HtmlWidgetViewController();

        [TestMethod]
        public void Can_Create_WidgetView_From_Widget()
        {
            Point location = new Point(0, 0);
            subject.Create(new HtmlWidget("New Widget 3", "http://widget-1.com", location));
            Assert.AreEqual(subject.Index[0].ActiveWidget.Name, "New Widget 3");
        }
    }
    #endregion

    #region Find
    [TestClass]
    public class FindWidgetView
    {
        HtmlWidgetViewController subject = new HtmlWidgetViewController();
        HtmlWidget testWidget;

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            testWidget = new HtmlWidget("Some Widget", "http://some-widget.com", location);
            subject.Create(testWidget);
            subject.Create(new HtmlWidget("Completely Different Widget", "http://different-widget.com", location));
        }

        [TestMethod]
        public void Can_Find_WidgetView()
        {
            List<HtmlWidgetView> results = subject.Find(testWidget);
            Assert.AreEqual(results.Count, 1);
        }
    }

    [TestClass]
    public class FindWidgetViewByMode
    {
        HtmlWidgetViewController subject = new HtmlWidgetViewController();
        Mode testMode;

        [TestInitialize]
        public void Setup()
        {
            testMode = new Mode("Test Mode");
            Point location = new Point(0, 0);
            testMode.Widgets.Create("Some Widget", "http://some-widget.com", location);
            testMode.Widgets.Create("Another Widget", "http://another-widget.com", location);

            subject.Create(testMode.Widgets.Find("Some Widget")[0]);
            subject.Create(testMode.Widgets.Find("Another Widget")[0]);
        }

        [TestMethod]
        public void Can_Find_WidgetsInMode()
        {
            List<HtmlWidgetView> results = subject.Find(testMode);
            Assert.AreEqual(results.Count, 2);
        }
    }
    #endregion

    #region Update
    [TestClass]
    public class UpdateWidgetView
    {
        HtmlWidgetViewController subject = new HtmlWidgetViewController();
        HtmlWidget targetWidget;
        HtmlWidgetView targetView;
        HtmlWidget newWidget;
        HtmlWidgetView newWidgetView;

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            targetWidget = new HtmlWidget("An Old Widget", "http://old-widget.com/", location);
            subject.Create(targetWidget);
            subject.Create(new HtmlWidget("A Similar Widget", "http://old-widget-s.com/", location));

            newWidget = new HtmlWidget("Another Widget", "http://another-widget.com/", location);

            targetView = subject.Find(targetWidget)[0];
            newWidgetView = new HtmlWidgetView(newWidget);

            subject.Update(targetView, newWidgetView);
        }

        [TestMethod]
        public void New_Widget_View_Made_It_Into_Index_After_Update()
        {
            Assert.AreEqual(subject.Find(newWidget).Count, 1);
        }

        [TestMethod]
        public void Old_Widget_View_Is_No_More_After_Update()
        {
            Assert.AreEqual(subject.Find(targetView.ActiveWidget).Count, 0);
        }

        [TestMethod]
        public void Index_Is_As_Long_As_Can_Be_Expected_After_Update()
        {
            Assert.AreEqual(subject.Index.Count, 2);
        }
    }
    #endregion

    #region Destroy
    [TestClass]
    public class DestroyWidgetView
    {
        HtmlWidgetViewController subject = new HtmlWidgetViewController();
        HtmlWidget testWidget;

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            testWidget = new HtmlWidget("An Old Widget", "http://old-widget.com/", location);
            subject.Create(testWidget);
            subject.Create(new HtmlWidget("A Similar Widget", "http://old-widget-s.com/", location));

            HtmlWidgetView targetWidgetView = subject.Find(testWidget)[0];
            subject.Destroy(targetWidgetView);
        }

        [TestMethod]
        public void Old_Widget_Is_No_More_After_Destroy()
        {
            Assert.AreEqual(subject.Find(testWidget).Count, 0);
        }

        [TestMethod]
        public void Index_Is_As_Long_As_Can_Be_Expected_After_Destroy()
        {
            Assert.AreEqual(subject.Index.Count, 1);
        }
    }

    [TestClass]
    public class DestroyAllWidgetViews
    {
        HtmlWidgetViewController subject = new HtmlWidgetViewController();

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            subject.Create(new HtmlWidget("Another Widget", "http://another-widget.com/", location));
            subject.Create(new HtmlWidget("A Similar Widget", "http://old-widget-s.com/", location));
        }

        [TestMethod]
        public void Index_Is_As_Long_As_Can_Be_Expected_After_Destroy_All_WidgetViews()
        {
            subject.DestroyAll();
            Assert.AreEqual(subject.Index.Count, 0);
        }
    }
    #endregion

}
