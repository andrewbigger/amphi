﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Amphi.Controllers;
using Amphi.Models;
using Amphi.Helpers;

namespace AmphiTest.Controllers
{
    #region Index
    [TestClass]
    public class WidgetIndex
    {
        HtmlWidgetController Subject = new HtmlWidgetController();

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            Subject.Create("New Widget", "http://widget-1.com", location);
            Subject.Create("New Widget 2", "http://widget-2.com", location);
        }

        [TestMethod]
        public void Has_Index_Of_Widgets()
        {
            Assert.AreEqual(Subject.Index.Count, 2);
        }
    }
    #endregion
    
    #region Create
    [TestClass]
    public class CreateWidget
    {
        HtmlWidgetController subject = new HtmlWidgetController();

        [TestMethod]
        public void Can_Create_Widget()
        {
            Point location = new Point(0, 0);
            subject.Create("New Widget 3", "http://widget-3.com", location);
            Assert.AreEqual(subject.Index[0].Name, "New Widget 3");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidWidgetNameException), "Invalid Name")]
        public void Does_Not_Create_Invalid_Widget()
        {
            Point location = new Point(0, 0);
            subject.Create(" ", "http://widget-3.com", location);
            Assert.AreEqual(subject.Index.Count, 0);
        }
    }
    #endregion

    #region Find
    [TestClass]
    public class FindWidget
    {
        HtmlWidgetController Subject = new HtmlWidgetController();

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            Subject.Create("Some Widget", "http://some-widget.com", location);
            Subject.Create("Completely Different Widget", "http://different-widget.com", location);
        }

        [TestMethod]
        public void Can_Find_Widget()
        {
            List<HtmlWidget> results = Subject.Find("Some Widget");
            Assert.AreEqual(results.Count, 1);
        }
    }
    #endregion

    #region Update
    [TestClass]
    public class UpdateWidget
    {
        HtmlWidgetController Subject = new HtmlWidgetController();

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0,0);
            Subject.Create("An Old Widget", "http://old-widget.com/", location);
            Subject.Create("Another Widget", "http://another-widget.com/", location);
            Subject.Create("A Similar Widget", "http://old-widget-s.com/", location);
            
            HtmlWidget TargetWidget = Subject.Find("An Old Widget")[0];
            HtmlWidget newWidget = new HtmlWidget("New Widget", "http://new-widget-url.com", location);
            Subject.Update(TargetWidget, newWidget);
        }

        [TestMethod]
        public void New_Widget_Made_It_Into_Index_After_Update()
        {
            Assert.AreEqual(Subject.Find("New Widget").Count, 1);
        }

        [TestMethod]
        public void Old_Widget_Is_No_More_After_Update()
        {
            Assert.AreEqual(Subject.Find("An Old Widget").Count, 0);
        }

        [TestMethod]
        public void Index_Is_As_Long_As_Can_Be_Expected_After_Update()
        {
            Assert.AreEqual(Subject.Index.Count, 3);
        }
    }
    #endregion

    #region Destroy
    [TestClass]
    public class DestroyWidget
    {
        HtmlWidgetController Subject = new HtmlWidgetController();

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0,0);
            Subject.Create("An Old Widget", "http://old-widget.com/", location);
            Subject.Create("Another Widget", "http://another-widget.com/", location);

            HtmlWidget TargetWidget = Subject.Find("An Old Widget")[0];
            Subject.Destroy(TargetWidget);
        }

        [TestMethod]
        public void Old_Widget_Is_No_More_After_Destroy()
        {
            Assert.AreEqual(Subject.Find("An Old Widget").Count, 0);
        }

        [TestMethod]
        public void Index_Is_As_Long_As_Can_Be_Expected_After_Destroy()
        {
            Assert.AreEqual(Subject.Index.Count, 1);
        }
    }

    [TestClass]
    public class DestroyAllWidget
    {
        HtmlWidgetController Subject = new HtmlWidgetController();

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            Subject.Create("An Old Widget", "http://old-widget.com/", location);
            Subject.Create("Another Widget", "http://another-widget.com/", location);
        }

        [TestMethod]
        public void Index_Is_As_Long_As_Can_Be_Expected_After_Destroy_All()
        {
            Subject.DestroyAll();
            Assert.AreEqual(Subject.Index.Count, 0);
        }
    }
    #endregion 

}
