﻿using Amphi.Helpers;
using Amphi.Models;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AmphiTest.Models
{
    #region Constructor
    [TestClass]
    public class ModeConstruction
    {
        [TestMethod]
        public void Create_Mode_With_Valid_Params()
        {
            Mode subject = new Mode("Test Mode");
            Assert.IsNotNull(subject);
        }
        
        [TestMethod]
        [ExpectedException(typeof(InvalidModeNameException), "Invalid Name")]
        public void Create_Mode_With_Invalid_Params()
        {
            Mode subject = new Mode(" ");
            Assert.IsNull(subject);
        }
    }
    #endregion
}
