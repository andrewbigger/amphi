﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Amphi.Models;
using Amphi.Helpers;

namespace AmphhiTest.Models
{
    #region Constructor
    [TestClass]
    public class WidgetConstruction
    {
        // Valid params
        [TestMethod]
        public void Create_Widget_With_Valid_Params()
        {
            Point location = new Point(0, 0);
            HtmlWidget Subject = new HtmlWidget("Monitor", "http://monitor.com", location);
            Assert.IsNotNull(Subject);
        }

        // Invalid params
        [TestMethod]
        [ExpectedException(typeof(InvalidWidgetNameException), "Invalid Name")]
        public void Create_Widget_With_Invalid_Name()
        {
            Point location = new Point(0, 0);
            HtmlWidget Subject = new HtmlWidget(" ", "http://monitor.com", location);
            Assert.IsNull(Subject);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidWidgetUrlException), "Invalid Url")]
        public void Create_Widget_With_Invalid_URL()
        {
            Point location = new Point(0, 0);
            HtmlWidget Subject = new HtmlWidget("Monitor", "monitor+com", location);
            Assert.IsNull(Subject);
        }

    }
    #endregion

    #region Public Properties
    [TestClass]
    public class NameProperty
    {
        private HtmlWidget Subject;

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            Subject = new HtmlWidget("name", "http://monitor.com", location);
        }

        [TestMethod]
        public void Update_Name_With_Valid_Name()
        {
            Subject.Name = "Another Name";
            Assert.AreEqual(Subject.Name, "Another Name");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidWidgetNameException), "Invalid Name")]
        public void Update_Name_With_Inalid_Name()
        {
            Subject.Name = "";
            Assert.AreEqual(Subject.Name, "name");
        }

    }

    [TestClass]
    public class GetUrlProperty
    {
        private HtmlWidget Subject;

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            Subject = new HtmlWidget("name", "http://monitor.com", location);
        }

        [TestMethod]
        public void Retrieve_Url()
        {
            Uri widgetUrl = Subject.GetUrl;
            Assert.AreEqual(widgetUrl.AbsoluteUri, "http://monitor.com/");
        }

    }

    [TestClass]
    public class SetUrlProperty
    {
        private HtmlWidget Subject;

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            Subject = new HtmlWidget("name", "http://monitor.com", location);
        }

        [TestMethod]
        public void Set_Url()
        {
            Subject.SetUrl = "http://another-monitor.com";
            Assert.AreEqual(Subject.GetUrl, "http://another-monitor.com/");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidWidgetUrlException), "Invalid Name")]
        public void Set_Invalid_Url()
        {
            Subject.SetUrl = "ftp://another-monitor.com";
            Assert.AreEqual(Subject.GetUrl, "http://monitor.com/");
        }

    }

    [TestClass]
    public class UrlEdgeCases
    {
        private HtmlWidget Subject;

        [TestInitialize]
        public void Setup()
        {
            Point location = new Point(0, 0);
            Subject = new HtmlWidget("name", "http://monitor.com", location);
        }

        [TestMethod]
        public void Create_Local_Host_Widget()
        {
            Point location = new Point(0, 0);
            HtmlWidget local = new HtmlWidget("localhost", "http://localhost:3000", location);
            Assert.AreEqual(local.GetUrl, "http://localhost:3000/");
        }

        [TestMethod]
        public void Update_Local_Host_Widget()
        {
            Subject.SetUrl = "http://localhost:9000";
            Assert.AreEqual(Subject.GetUrl, "http://localhost:9000/");
        }

        [TestMethod]
        public void Create_Widget_With_Get_Params()
        {
            Point location = new Point(0, 0);
            HtmlWidget local = new HtmlWidget("localhost", "http://another-monitor.com/en-us/library/0yd65esw.aspx?something=something", location);
            Assert.AreEqual(local.GetUrl, "http://another-monitor.com/en-us/library/0yd65esw.aspx?something=something");
        }

        [TestMethod]
        public void Update_Widget_With_Get_Params()
        {
            Subject.SetUrl = "http://another-monitor.com/index.aspx?another-thing=something";
            Assert.AreEqual(Subject.GetUrl, "http://another-monitor.com/index.aspx?another-thing=something");
        }

        [TestMethod]
        public void Create_Widget_With_Port()
        {
            Point location = new Point(0, 0);
            HtmlWidget local = new HtmlWidget("localhost", "http://another-monitor.com:8080/en-us/library/0yd65esw.aspx?something=something", location);
            Assert.AreEqual(local.GetUrl, "http://another-monitor.com:8080/en-us/library/0yd65esw.aspx?something=something");
        }

        [TestMethod]
        public void Update_Widget_With_Port()
        {
            Subject.SetUrl = "http://another-monitor.com:8080/index.aspx?another-thing=something";
            Assert.AreEqual(Subject.GetUrl, "http://another-monitor.com:8080/index.aspx?another-thing=something");
        }
    }

    #endregion

}
